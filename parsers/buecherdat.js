const fs = require('fs');
const moment = require('moment');
var vm = {};
var db;

const ParserAntwort = require('./datenbankValidators.js').ParserAntwort;

//string / null
vm.parseDate = function(date) {
	if(!date || date === 'NL') {
		const now = moment();
		return now.format('YYYY-MM-DD');
	}
	var teile = date.split('.');
	if(teile.length != 3) {
		return null;
	}
	if(teile[2].length === 2) {
		//Jahr anhand 2 statt 4 Ziffern angegeben
		//Beispiel: '04' => '2004'
		teile[2] = '20' + teile[2];
	}
	return teile[2] + '-' + teile[1] + '-' + teile[0];
}

//Int / null
vm.bezeichnungZuStufe = function(klasse) {
	if(isNaN(+klasse)) {
		//klasse ist nicht ausschließlich numerisch
		switch(klasse) {
			case 'J1':
			return 11;
			case 'J2':
			return 12;
			case 'L1':
			return 11;
			default:
			return null;
		}
	} else {
		return +klasse;
	}
}

vm.parseLine = function(line) {
	const parts = line.split('\\');
	const stufeVon = vm.bezeichnungZuStufe(parts[1]);
	const stufeBis = vm.bezeichnungZuStufe(parts[2]);
	const eingabedatum = vm.parseDate(parts[9]);
	if(!stufeVon || !stufeBis || !eingabedatum) {
		console.log(parts);
		return null;
	}
	return {
		alte_id: parts[0],
		kurs_kuerzel: parts[0].substr(0, 2),
		stufeVon: stufeVon,
		stufeBis: stufeBis,
		titel: parts[3],
		verlag: parts[4],
		isbn: parts[5],
		preis: parts[6].replace(',', '.'),
		//bsp: *FF, ???
		//bsp: NL, ???
		eingabedatum: eingabedatum
	};
}


vm.seperateLines = function(contents, callback) {
	var lines = contents.split('\r\n').filter(line => line.length !== 0);
	return lines;
}

vm.checkIfValid = function(buch) {
	/*
	* Regeln:
	* - ID darf noch nicht existieren
	* - Fach mit Kurs-ID muss existieren
	* 
	*/
	return new Promise((resolve, reject) => {
		var _buch = buch;
		db.Buch.findAll({
			where: {
				alte_id: _buch.alte_id
			}
		})
		.then(ergebnisse => {
			if(ergebnisse.length !== 0) {
				resolve({buch: _buch, okay: false});
			} else {
				db.Fach.findAll({
					where: {
						kuerzel: _buch.kurs_kuerzel
					}
				})
				.then(faecher => {
					if(faecher.length !== 1) {
						resolve({buch: _buch, okay: false});
					} else {
						_buch.fach_id = faecher[0].uuid;
						resolve({buch: _buch, okay: true});
					}
				})
			}
		})
		.catch(fehler => {
			reject(fehler);
		})
	})
}

vm.parseFile = function(filePath, _db, /*boolean:*/importFile) {
	db = _db;
	var p = new Promise(function(resolve, reject) {
		fs.readFile(filePath, (error, buffer) => {
			if(error) {
				console.log('err');
				return reject('filenotfound');
			} else {
				const contents = buffer.toString();
				const lines = vm.seperateLines(contents);
				var valideBuecher = [];
				var parserFehler = [];
				var datenbankFehler = [];
				var queries = [];
				for(var i=0;i<lines.length;i++) {
					const buch = vm.parseLine(lines[i]);
					if(!buch) {
						parserFehler.push(lines[i]);
					} else {
						queries.push(vm.checkIfValid(buch));
					}
				}
				Promise.all(queries)
				.then(ergebnisse => {
					console.log('Promise.all');
					for(var i=0;i<ergebnisse.length;i++) {
						var _ergebniss = ergebnisse[i];
						if(_ergebniss.okay) {
							valideBuecher.push(_ergebniss.buch);
							console.log('valideBuecherPush:', _ergebniss.buch);
						} else {
							datenbankFehler.push(_ergebniss.buch);
						}
					}
					/*
					 * Buch-Objekt:
					 * alte_id: parts[0],
					 * kurs_kuerzel: parts[0].substr(0, 2),
					 * fach_id: (wird von vm.checkIfValid hinzugefügt sofern Fach existiert,
					 * 	   diese muss existieren => keine weitere Überprüfung notwendig)
					 * stufeVon: stufeVon,
					 * stufeBis: stufeBis,
					 * titel: parts[3],
					 * verlag: parts[4],
					 * isbn: parts[5],
					 * preis: parts[6],
					 * eingabedatum: eingabedatum
					 */
					if(importFile) {
						var buecherErstellenQueries = [];
						for(var i=0;i<valideBuecher.length;i++) {
							var buchZuImportieren = valideBuecher[i];
							console.log('buchZuImportieren:', buchZuImportieren);
							var neuesBuch = db.Buch.build({
								alte_id: buchZuImportieren.alte_id,
								titel: buchZuImportieren.titel,
								verlag: buchZuImportieren.verlag,
								preis: buchZuImportieren.preis,
								stufe_von: buchZuImportieren.stufeVon,
								stufe_bis: buchZuImportieren.stufeBis,
								eingabedatum: buchZuImportieren.eingabedatum,
								isbn: buchZuImportieren.isbn,
								bestand: 0,
								fach_id: buchZuImportieren.fach_id
							});
							buecherErstellenQueries.push(neuesBuch.save());
						}
						Promise.all(buecherErstellenQueries)
						.then(_ => {
							resolve();
						})
						.catch(fehler => {
							console.log(fehler);
							reject('Datenbank-Fehler');
						})
					} else {
						//sende Daten an Client
						const antwort = new ParserAntwort(parserFehler, datenbankFehler, valideBuecher);
            			resolve(antwort);
					}
				})
				.catch(_fehler => {
					reject(_fehler.toString());
				})
			}
		});
	})
	return p;
}
module.exports = vm;