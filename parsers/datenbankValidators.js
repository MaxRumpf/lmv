var Datenbank;

module.exports.ParserAntwort = class ParserAntwort {
    constructor(parserFehler, DatenbankFehler, valideObjekte) {
        this.parserFehler = parserFehler;
        this.datenbankFehler = DatenbankFehler;
        this.valideObjekte = valideObjekte;
    }
}

module.exports.initialisieren = function(DatenbankObjekt) {
    Datenbank = DatenbankObjekt;
}

module.exports.überprüfeFachExistenz = function(typ, kürzel) {
    return new Promise((resolve, reject) => {
        if (kürzel === '-') {
            return resolve({typ: typ+'_id', wert: ''});
        }
        if(!typ || !kürzel) {
            console.log(typ.toString() + kürzel.toString());
            return reject('a');
        }
        Datenbank.Fach.findAll({
            where: {
                typ: typ,
                kuerzel: kürzel
            }
        })
        .then(ergebnisse => {
            if(ergebnisse.length !== 1 && kürzel !== '-') {
                console.log(typ, kürzel);
                return reject('b');
            }
            return resolve({typ: typ+'_id', wert: ergebnisse[0].uuid});
        })
        .catch(f => {
            return reject(f);
        })
    })
}

module.exports.überprüfeSchülerNichtDuplikat = function(name, geburtsdatum, klasseBezeichnung, wohnort) {
    return new Promise((resolve, reject) => {
        if(!name || !geburtsdatum || !klasseBezeichnung) {
            return reject();
        }
        Datenbank.Klasse.findAll({
            where: {
                bezeichnung: klasseBezeichnung
            }
        })
        .then(klassenErgebnisse => {
            if (klassenErgebnisse.length !== 1) {
                return reject();
            }
            return resolve({typ: 'klasse_id', wert: klassenErgebnisse[0].uuid});
        })
        .catch(f => {
            return reject();
        })
    })
}