const fs = require('fs');
const moment = require('moment');
const xlsx = require('xlsx');
const datenbankValidator = require('./datenbankValidators.js');
var vm = {};
var datenbank;

//Fach-Kürzel für jede Bezeichnung in schueler.xls
//Nach Absprache mit Sekretariat
const EINSTELLUNGEN = {
    fremdspracheBezeichnungen: {
        'L': 'LA',
        'F': 'FR',
        '': '-'
    },
    profilBezeichnungen: {
        'Naturwissenschaftliches Profil': 'NW',
        'Sprachliches Profil': 'ES',
        'Musik-Profil': 'MP',
        'Standard 5 bis 7': '-'
    },
    konfessionBezeichnungen: {
        'rk': 'RK',
        'Ethik': 'ET',
        'ev': 'RE'
    }
};

vm.nullPadding = function(s) {
    if(s === '11') {return 'J1'}
    if(s === '12') {return 'J2'}
	var zahlenString = '';
	var buchstabenString = '';
	for(var i=0;i<s.length;i++) {
		var c = s.charAt(i);
		if(!isNaN(c)) {
			zahlenString = zahlenString + c;
		} else {
			break;
		}
	}
	buchstabenString = s.replace(zahlenString, '');
	while (zahlenString.length < (2)) {zahlenString = "0" + zahlenString;}
	return zahlenString + buchstabenString;
}

vm.fremdspracheString = function(s) {
    return EINSTELLUNGEN.fremdspracheBezeichnungen[s]? EINSTELLUNGEN.fremdspracheBezeichnungen[s] : null;
}

vm.profilString = function(s) {
    return EINSTELLUNGEN.profilBezeichnungen[s]? EINSTELLUNGEN.profilBezeichnungen[s] : null;
}

vm.konfessionString = function(s) {
    return EINSTELLUNGEN.konfessionBezeichnungen[s]? EINSTELLUNGEN.konfessionBezeichnungen[s] : null;
}

vm.parseDate = function(date) {
	if(!date || date === 'NL') {
		return null;
	}
	var teile = date.split('.');
	if(teile.length != 3) {
		return null;
	}
	return teile[2] + '-' + teile[1] + '-' + teile[0];
}

vm.getCellContent = function(row, column, worksheet) {
    var cell = worksheet[this.addressFromCoords(row, column)];
    return cell? cell.v : '';
}

vm.addressFromCoords = function(row, column) {
    const a = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P'][row];
    const b = column.toString();
    return a+b;
}

vm.parseLine = function(i, worksheet) {
    var s = {
        name: this.getCellContent(0, i, worksheet) + ', ' + this.getCellContent(1, i, worksheet),
        geburtsdatum: this.parseDate(this.getCellContent(6, i, worksheet)),
        klasse: vm.nullPadding(this.getCellContent(2, i, worksheet)),
        fremdsprache: this.fremdspracheString(this.getCellContent(4, i, worksheet)),
        profil: this.profilString(this.getCellContent(8, i, worksheet)),
        konfession: this.konfessionString(this.getCellContent(10, i, worksheet))
    };
    for(var k in s) {
        if(k !== 'profil' && k !== 'fremdsprache' && s.hasOwnProperty(k) && s[k] === null) {
            console.log(s);
            return null;
        }
    }
    return s;
}

class SchülerValiditätAntwort {
    constructor(schüler, valide) {
        this.schüler = schüler;
        this.valide = valide;
    }
    istValide() {
        return this.valide;
    }
    istInvalide() {
        return !this.valide;
    }
}

vm.überprüfeSchüler = function(schüler) {
    return new Promise((resolve, reject) => {
        const schülerName = schüler.name;
        const schülerGeburtsdatum = schüler.geburtsdatum;
        const schülerKlasse = schüler.klasse;
        const schülerFremdsprache = schüler.fremdsprache;
        const schülerProfil = schüler.profil;
        const schülerKonfession = schüler.konfession;
        
        var bedingungen = [];
        
        bedingungen.push( datenbankValidator.überprüfeFachExistenz('fremdsprache', schülerFremdsprache) );
        bedingungen.push( datenbankValidator.überprüfeFachExistenz('profil', schülerProfil?schülerProfil:'-') );
        bedingungen.push( datenbankValidator.überprüfeFachExistenz('konfession', schülerKonfession) );
        
        schüler.konfession_id = '';
        schüler.fremdsprache_id = '';
        schüler.profil_id = '';

        Promise.all(bedingungen)
        .then(antworten => {
            //alles in Ordnung
            antworten.forEach(a => {
                if(!a) {
                    console.log('Fach existiert nicht:', schüler);
                    return;
                }
                switch(a.typ) {
                    case 'konfession_id':
                    schüler.konfession_id = a.wert;
                    break;case 'fremdsprache_id':
                    schüler.fremdsprache_id = a.wert;
                    break;case 'profil_id':
                    schüler.profil_id = a.wert;
                    break;
                    default: break;
                }
            });
            datenbank.Klasse.findAll({
                where: {
                    bezeichnung: schüler.klasse
                }
            })
            .then(klassenErgebnisse => {
                if (klassenErgebnisse.length !== 1) {
                    const antwort = new SchülerValiditätAntwort(schüler, false);
                    console.log('Klasse existiert nicht:', schüler);
                    resolve(antwort);
                } else {
                    schüler.klasse_id = klassenErgebnisse[0].uuid;
                    const antwort = new SchülerValiditätAntwort(schüler, true);
                    resolve(antwort);
                }
                
            })
        })
        .catch(f => {
            console.log('Schüler nicht valide: ', f);
            //eine/alle der Bedingungen wurden nicht erfüllt
            const antwort = new SchülerValiditätAntwort(schüler, false);
            resolve(antwort);
        })
    });
}

vm.parseLines = function(workbook) {
    return new Promise((resolve, reject) => {
        var schueler = [];
        var worksheet = workbook.Sheets[workbook.SheetNames[0]];
        var i = 2;
        var datenFehler = [];
        var überprüfeSchülerQueries = [];
        while(this.getCellContent(0, i, worksheet) !== '') {
            const s = this.parseLine(i, worksheet);
            if(s === null) {
                datenFehler.push('Zeile ' + i);
            } else {
                überprüfeSchülerQueries.push(vm.überprüfeSchüler(s));
            }
            i++;
        }
        Promise.all(überprüfeSchülerQueries)
        .then(ergebnisse => {
            var datenbankFehler = [];
            var valideSchüler = [];
            datenbankFehler = ergebnisse.filter(e => e.istInvalide()).map(e => e.schüler);
            valideSchüler = ergebnisse.filter(e => e.istValide()).map(e => e.schüler);
            const antwort = new datenbankValidator.ParserAntwort(datenFehler, datenbankFehler, valideSchüler);
            resolve(antwort);
        })
    });
}

vm.speichereSchüler = function(valideSchüler) {
    return new Promise((resolve, reject) => {
        var insertQueries = [];
        valideSchüler.forEach(s => {
            insertQueries.push(new Promise((res1, rej1) => {
                datenbank.Schueler.findAll({
                    where: {
                        geburtsdatum: s.geburtsdatum,
                        name: s.name
                    }
                })
                .then(schuelerErgebnisse => {
                    if(schuelerErgebnisse.length === 0) {
                        datenbank.Schueler.build(s).save()
                        .then(_ => {res1()})
                        .catch(f => {rej1(f)})
                    } else {
                        var klasseÄndernQueries = [];
                        for(var i=0;i<schuelerErgebnisse.length;i++) {
                            klasseÄndernQueries.push(schuelerErgebnisse[i].update({klasse_id: s.klasse_id}));
                        }
                        Promise.all(klasseÄndernQueries)
                        .then(_ => {res1();})
                        .catch(f => {rej1(f)})
                    }
                })
                .catch(f => {rej1(f)})
            }));
        })
        Promise.all(insertQueries)
        .then(_ => {
            resolve();
        })
        .catch(f => {
            console.log('importfehler:', f);
            reject(f ? f.toString() : 'Fehler beim Erstellen der Schüler in Datenbank');
        })
    })
}

vm.parseFile = function(path, datenbankObjekt, importFile) {
    datenbank = datenbankObjekt;
    datenbankValidator.initialisieren(datenbankObjekt);
    return new Promise(function (resolve, reject) {
        var workbook = xlsx.readFile(path);
        if(!workbook || !workbook.Sheets[workbook.SheetNames[0]]) {
            return reject('Datei hat keine Tabellen');
        }
        vm.parseLines(workbook)
        .then(ergebniss => {
            if(importFile) {
                vm.speichereSchüler(ergebniss.valideObjekte)
                .then(_ => {resolve()})
                .catch(f => {reject(f)});
            } else {
                return resolve(ergebniss);
            }
        })
        .catch(f => {
            console.log('vm.parseFile:äußeres Catch:', f);
            return reject();
        })
    })
}

module.exports = vm;