const fs = require('fs');
const moment = require('moment');
var vm = {};
var db;

const ParserAntwort = require('./datenbankValidators.js').ParserAntwort;

//string / null
vm.parseDate = function(date) {
	if(!date) {
		return null;
	}
	if(date.length != 8) {
		return null;
	}
	if(isNaN(+date)) {
		return null;
	}
	const jahr = date.substr(0, 4);
	const monat = date.substr(4,2);
	const tag = date.substr(6,2);
	return jahr + '-' + monat + '-' + tag;
}

vm.übersetzeKonfession = function(konfessionBezeichnung) {
	switch(konfessionBezeichnung) {
		case 'Ethik': return 'ET';
		case 'rk': return 'RK';
		case 'ev': return 'RE';
		case 'nichts': return '';
		default: return '';
	}
}

vm.übersetzeFremdsprache = function(fremdspracheBezeichnung) {
	switch(fremdspracheBezeichnung) {
		case 'L': return 'LA';
		case 'F': return 'FR';
		default: return '';
	}
}

vm.übersetzeProfil = function(profilBezeichnung) {
	switch(profilBezeichnung) {
		case 'M': return 'MP';
		case 'S': return 'ES';
		case 'N': return 'NW';
		default: return '';
	}
}

vm.parseLine = function(line) {
	const parts = line.split('\\');
	var data = {
		name: parts[0].split(' ')[0] + ', ' + parts[0].split(' ').slice(1).join(' '),
		geburtsdatum: vm.parseDate(parts[1]),
		wohnort: parts[2],
		klasse: parts[3],
		konfession: vm.übersetzeKonfession(parts[4]),
		fremdsprache: vm.übersetzeFremdsprache(parts[5][1]),
		profil: vm.übersetzeProfil(parts[5][2]),
		ausleihen: []
	};
	if(!data.geburtsdatum) {
		return null;
	}
	if(parts.length > 6) {
		for(var i=6;i<parts.length;i++) {
			if(parts[i] !== "") {
				data.ausleihen.push(parts[i]);
			}
		}
	}
	return data;
}


vm.seperateLines = function(contents, callback) {
	var lines = contents.split('\r\n').filter(line => line.length !== 0);
	return lines;
}

vm.analysiereSchuelerObjekt = function(_objekt) {
	var objekt = _objekt;
	/*
	* Constraints:
	* Geburtsdatum: existiert
	* Wohnort: existiert
	* Klasse: existiert
	* Konfession, Fremdsprache, Profil: existiert
	* 
	*/
	var p = new Promise((resolve, reject) => {
		db.Klasse.findAll({
			where: {
				bezeichnung: objekt.klasse
			}
		})
		.then(klassen => {
			if(klassen.length !== 1) {
				console.log('Klasse nicht gefunden:', objekt.klasse);
				return resolve({objekt: objekt, valide: false});
			}
			objekt.klasse_id = klassen[0].uuid;
			if(!objekt.geburtsdatum || !objekt.wohnort) {
				consolel.log('3');
				return resolve({objekt: objekt, valide: false});
			}
			db.Schueler.findAll({
				where: {
					name: objekt.name,
					geburtsdatum: objekt.geburtsdatum
				}
			})
			.then(schuelerMitGleichemNameGebursdatumWohnort => {
				if(schuelerMitGleichemNameGebursdatumWohnort.length > 0) {
					console.log('2');
					return resolve({objekt: objekt, valide: false});
				}
				Promise.all([
					db.Fach.findAll({where: {kuerzel: objekt.konfession?objekt.konfession:'ET', typ: 'konfession'}}),
					db.Fach.findAll({where: {kuerzel: objekt.profil?objekt.profil:'NW', typ: 'profil'}}),
					db.Fach.findAll({where: {kuerzel: objekt.fremdsprache, typ: 'fremdsprache'}})
				])
				.then(antworten => {
					for(let i=0;i<antworten.length;i++) {
						if(antworten[i].length !== 1) {
							console.log('1');
							console.log(objekt);
							resolve({objekt: objekt, valide: false});
							return;
						}
						switch(antworten[i][0].typ) {
							case 'konfession':
							objekt.konfession_id = antworten[i][0].uuid;
							break;
							case 'profil':
							objekt.profil_id = antworten[i][0].uuid;
							break;
							case 'fremdsprache':
							objekt.fremdsprache_id = antworten[i][0].uuid;
							break;
						}
					}
					resolve({objekt: objekt, valide: true});
				})
			})
		})
		.catch(_ => {
			reject({objekt: objekt, valide: false});
		})
	});
	return p;
}

vm.parseFile = function(path, _db, importFile) {
	db = _db;
	var p = new Promise(function (resolve, reject) {
		fs.readFile(path, (error, buffer) => {
			if(error) {
				reject(error);
			} else {
				const contents = buffer.toString();
				const lines = vm.seperateLines(contents);
				var valideSchueler = [];
				var fehlerhafteZeilen = [];
				var datenbankFehler = [];
				var queries = [];
				for(var i=0;i<lines.length;i++) {
					const aktuellerSchueler = vm.parseLine(lines[i]);
					if(!aktuellerSchueler) {
						fehlerhafteZeilen.push(lines[i]);
					} else {
						queries.push(vm.analysiereSchuelerObjekt(aktuellerSchueler));
					}
				}
				Promise.all(queries)
				.then(ergebnisse => {
					for(let i=0;i<ergebnisse.length;i++) {
						var _ergebniss = ergebnisse[i];
						if(_ergebniss.valide) {
							valideSchueler.push(_ergebniss.objekt);
						} else {
							datenbankFehler.push(_ergebniss.objekt);
						}
					}
					/*
					* name: parts[0].split(' ')[0] + ', ' + parts[0].split(' ').slice(1).join(' '),
					* geburtsdatum: vm.parseDate(parts[1]),
					* wohnort: parts[2],
					* klasse: parts[3],
					* konfession: vm.übersetzeKonfession(parts[4]),
					* fremdsprache: vm.übersetzeFremdsprache(parts[5][1]),
					* profil: vm.übersetzeProfil(parts[5][2]),
					* ausleihen: []
					*/
					if(importFile) {
						var schuelerQueries = [];
						for(var i=0;i<valideSchueler.length;i++) {
							const schueler = valideSchueler[i];
							var p = new Promise((schuelerResolve, schuelerReject) => {
								var _schueler = schueler;
								console.log('Ausleih.dat-Importer: erstelle neuen Schüler:', _schueler.name);
								db.Schueler.build({
									name: _schueler.name,
									geburtsdatum: _schueler.geburtsdatum,
									profil_id: _schueler.profil_id,
									konfession_id: _schueler.konfession_id,
									fremdsprache_id: _schueler.fremdsprache_id,
									klasse_id: _schueler.klasse_id
								})
								.save()
								.then(datenbankSchueler => {
									const uuid = datenbankSchueler.uuid;
									var ausleiheQueries = [];
									for(var a=0;a<_schueler.ausleihen.length;a++) {
										var _ausleihe = _schueler.ausleihen[a];
										var p2 = new Promise((ausleiheResolve, ausleiheReject) => {
											const alteBuchID = _ausleihe.slice(0, _ausleihe.length - 1);
											db.Buch.findAll({where: {alte_id: alteBuchID}})
											.then(buchMitID => {
												if(buchMitID.length !== 1) {
													return ausleiheReject('buchMitID.length !== 1');
												}
												console.log('Ausleih.dat-Importer: erstelle Ausleihe für Schüler:', _schueler.name, buchMitID[0].titel);
												db.Ausleihe.build({
													schueler_id: uuid,
													buch_id: buchMitID[0].uuid
												})
												.save()
												.then(_ => {
													ausleiheResolve();
												})
												.catch(f => {
													ausleiheReject(f.toString());
												})
											})
										});
										ausleiheQueries.push(p2);
									}
									Promise.all(ausleiheQueries)
									.then(_ => {
										schuelerResolve();
									})
									.catch(f => {
										
									});
								})
								.catch(f => {
									reject(f.toString());
								})
							});
							schuelerQueries.push(p);
						}
						Promise.all(schuelerQueries)
						.then(_ => {resolve()})
						.catch(f => {reject(f)})
					} else {
						const antwort = new ParserAntwort(fehlerhafteZeilen, datenbankFehler, valideSchueler);
            			resolve(antwort);
					}
				})
			}
		});
	});
	return p;
}


module.exports = vm;