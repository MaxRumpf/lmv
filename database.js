/*  
    LMV-Server arbeitet mit einem ORM (Object Relation Model).
    Die Datenbank-Operationen werden also nicht direkt durch traditionelle SQL-Queries erstellt,
    sondern durch eine Objekt-abstrahierung, um nicht Fehleranfälligkeit durch 'nackte' SQL-Queries zu haben
    und das Escaping-Problem zu umgehen.
    Diese Datenbank-Objekte werden hier definiert.
*/
//ORM-Plugin wird importiert
const Sequelize = require('sequelize');

//Datenbank-Plugin wird initialisiert und konfiguriert
//Bei Änderung des Datenbank-Datei-Namens hier eintragen
var sequelize;
sequelize = new Sequelize('lmv', '', '', {
  dialect: 'sqlite',
  storage: './datenbank.sqlite',
  operatorsAliases: false,
  logging: false
});

/*

    Hier werden nun die Datenbank-Modelle definiert.
    Eine Definition folgt folgendem Muster:
    
    const NameDesObjekts = sequelize.define('namekleingeschrieben', {
      ParameterDesObjekts
    }, {
      Einstellungen
    });

    Einem Modell werden Werte wie folgt hinzugefügt:
      wert1: TYP
    (hierbei ist TYP eine der von Sequelize definierten Konstanten, siehe Sequelize API Docs)
    oder
      wert2: {KonfigurationDesWertes}
    Letzteres wird in komplexeren Fällen benutzt falls der Wert ein Primary Key ist oder spezifischen Null-Wert haben soll.
    Konfigurationsoptionen wieder siehe Sequelize API Docs...

*/
const Buch = sequelize.define('buch', {
  uuid: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  alte_id: Sequelize.STRING,
  titel: Sequelize.STRING,
  verlag: Sequelize.STRING,
  preis: Sequelize.FLOAT(5,2),
  stufe_von: Sequelize.INTEGER,
  stufe_bis: Sequelize.INTEGER,
  eingabedatum: Sequelize.DATEONLY,
  isbn: Sequelize.STRING,
  bestand: Sequelize.INTEGER,
  fach_id: {
    type: Sequelize.UUID,
    allowNull: false
  }
}, {
  freezeTableName: true,
  tableName: 'buecher'
});

const Schueler = sequelize.define('schueler', {
  uuid: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  name: Sequelize.STRING,
  geburtsdatum: Sequelize.DATEONLY,
  profil_id: {
    type: Sequelize.UUID,
    allowNull: true
  },
  konfession_id: {
    type: Sequelize.UUID,
    allowNull: true
  },
  fremdsprache_id: {
    type: Sequelize.UUID,
    allowNull: true
  },
  klasse_id: {
    type: Sequelize.UUID,
    allowNull: false
  }
}, {
  freezeTableName: true,
  tableName: 'schueler'
});

const Fach = sequelize.define('fach', {
  uuid: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  //Typ: Normal, Profil ab 6., Konfession, Fremdsprache ab 5.
  typ: Sequelize.STRING,
  stufe_von: Sequelize.INTEGER,
  stufe_bis: Sequelize.INTEGER,
  bezeichnung: Sequelize.STRING,
  kuerzel: Sequelize.STRING
}, {
  freezeTableName: true,
  tableName: 'faecher'
});

const Klasse = sequelize.define('klasse', {
  uuid: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  bezeichnung: Sequelize.STRING,
  stufe: Sequelize.INTEGER
}, {
  freezeTableName: true,
  tableName: 'klasse'
});

// Ausleihe verknüpft ein Buch mit einem Schüler. Sofern dieses Objekt für einen Schüler und einem Buch existiert, so hat er dieses ausgeliehen.
const Ausleihe = sequelize.define('ausleihe', {
  uuid: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  schueler_id: Sequelize.UUID,
  buch_id: Sequelize.UUID
}, {
  freezeTableName: true,
  tableName: 'ausleihen'
});

// Deputate weisen einen Lehrer einem Fach in einer Klasse zu.
const Deputat = sequelize.define('deputat', {
  uuid: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  fach_id: Sequelize.UUID,
  klasse_id: Sequelize.UUID,
  schueler_id: Sequelize.UUID
}, {
  freezeTableName: true,
  tableName: 'deputate'
});

const Einstellung = sequelize.define('einstellung', {
  uuid: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  schluessel: Sequelize.STRING,
  wert: Sequelize.STRING
}, {
  freezeTableName: true,
  tableName: 'einstellungen'
});

//erstelle db-Objekt welches wir nach außen verfügbar machen
var db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.Klasse = Klasse;
db.Buch = Buch;
db.Fach = Fach;
db.Schueler = Schueler;
db.Ausleihe = Ausleihe;
db.Deputat = Deputat;
db.Einstellung = Einstellung;

// Hier wird die Verbindung zur Datenbank hergestellt.
// Bei Fehlern siehe Sequelize API Docs
db.starteVerbindung = function(cb) {
  sequelize.sync().then(() => {
    sequelize.authenticate().then(() => {
      cb(false);
    }).catch(() => {
      cb('Fehler: Verbindung zu SQLite3 fehlgeschlagen.');
    });
  }).catch((f) => {
    console.log(f);
    cb('Fehler: Synchronisation von Tabellen fehlgeschlagen.');
  });
}

// Die erstellten Objekte werden von diesem Modul nach aussen hin zur Verfügung gestellt
module.exports = db;