const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const db = require('./database.js');
const config = require('./config.js');

//Erstelle app Objekt
const app = express();

//Konfiguriere die Middlewares z.B. für Logging und den 'public'-Ordner
app.use(bodyParser.json());
app.use(express.static('public'));

//Importiere Routen aus /routes/
app.use('/faecher', require('./routes/fach.js')(db));
app.use('/schueler', require('./routes/schueler.js')(db));
app.use('/klassen', require('./routes/klassen.js')(db));
app.use('/buecher', require('./routes/buecher.js')(db));
app.use('/deputate', require('./routes/deputate.js')(db));
app.use('/ausleihe', require('./routes/ausleihe.js')(db));
app.use('/dateiupload', require('./routes/dateiupload.js')(db));
app.use('/einstellungen', require('./routes/einstellungen.js')(db));
app.use('/utils', require('./routes/utils.js')(db));

//Bereite Datenbank auf Benutzung vor => Erstelle/Verändere Tabellenstruktur
db.starteVerbindung(function(fehler) {
  if(fehler) {
    return console.log(fehler);
  }
  console.log('Verbindung zu Datenbank erfolgreich hergestellt')
  app.listen(config.webserverPort);
  console.log('Server auf Port ' + config.webserverPort.toString() + ' gestartet');
});