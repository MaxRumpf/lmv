// Dies ist ein Einstellungs-Modul
// Einzige Einstellung im Moment ist die Konstante config.webserverPort [int]
// Bei gewünschtem Portwechsel einfach ändern
var config = {
	webserverPort: 80
};

// Config-Objekt wird nach aussen hin zur Vefügung gestellt
module.exports = config;