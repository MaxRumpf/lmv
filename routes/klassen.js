const express = require('express');


const exp = function(db) {
	const router = express.Router();
	// Liest alle Klassen aus
    // Eingabewert: nichts
    // Ausgabewert: JSON: {error: true} oder [klasse1, klasse2, ...]
	router.get('/', function(req, res) {
		var klassen = db.Klasse.findAll().then(klassen => {
			res.json(klassen);
		}).catch(err => {
			res.json({error: 'true', reason: 'Datenbank'});
			console.log('Fehler: Klassen /get konnte Klassen nicht aus Datenbank auslesen');
			console.log(err);
		});
	});
	// liest Anzahl der Schüler in jeder Klasse aus
    // Eingabewert: nichts
    // Ausgabewert: JSON: {error: true} oder {error: false, data: [{klasse: klasseObjekt, anzahl: Zahl}, ...]}
	router.get('/anzahlen', function(req, res) {
		db.Klasse.findAll().then(klassen => {
			console.log(klassen.length);
			var queries = [];
			
			for(var i=0;i<klassen.length;i++) {
				var klasse = klassen[i];
				queries.push(new Promise((resolve, reject) => {
					var _klasse = klasse;
					db.Schueler.count({where: {klasse_id: _klasse.uuid}})
					.then(anzahl => {
						resolve({klasse: _klasse, anzahl: anzahl});
					})
					.catch(fehler => {
						reject(fehler);
					});
				}));
			}

			Promise.all(queries)
			.then(antworten => {
				res.json({error: false, data: antworten});
			})
			.catch(fehler => {
				console.log(fehler);
				res.json({error: true});
			});

		});
	});
	// Erstellt neue Klasse
    // Eingabewert: POST-Daten bezeichnung stufe
    // Ausgabewert: JSON: {error: true} oder {error: false, id: erstelltesFachUUID}
	router.post('/', function(req, res) {
		//express speichert POST-daten in req.body
		const data = req.body;
		//extrahiere die Daten (unnötig aber lässt raum für weitere validierung)
		const bezeichnung = data.bezeichnung;
		const stufe = data.stufe;

		const neueKlasse = db.Klasse.build({
			bezeichnung: bezeichnung,
			stufe: stufe
		});
		neueKlasse.save().then(erstelltesObjekt => {
			//konnte abgespeichert werden
			res.json({error: false, id: erstelltesObjekt.uuid});
		}).catch(err => {
			res.json({error: true});
			console.log('Fehler: POST / konnte Klasse nicht abspeichern');
			console.log(err);
		});
	});
	// aktualisiert Daten eines Faches (veränderte Bezeichnung, Stufe)
    // Eingabewert: URL-Daten: uuid | POST-Daten bezeichnung stufe
    // Ausgabewert: JSON: {error: true} oder {error: false}
	router.put('/:uuid', function(req, res) {
		const data = req.body;
		db.Klasse.findOne({where: {
			uuid: req.params.uuid}
		}).then(klasse => {
			klasse.update(data).then(_ => {
				res.json({error: false});
			});
		}).catch(fehler => {
			res.json({error: true});
			console.log('Fehler: PUT / konnte Klasse nicht aktualisieren');
			console.log(fehler);
		});
	});
	// löscht Klasse
    // Eingabewert: URL-Daten: uuid
    // Ausgabewert: JSON: {error: true} oder {error: false}
	router.delete('/:uuid', function(req, res) {
		db.Schueler.findAll({where: {klasse_id: req.params.uuid}})
		.then(schueler => {
			if(schueler.length !== 0) {
				return res.json({error: true, grund: 'noch Schüler in Klasse'});
			}
			db.Klasse.findOne({where: {
				uuid: req.params.uuid}
			}).then(klasse => {
				klasse.destroy().then(_ => {
					res.json({error: false});
				});
			}).catch(fehler => {
				res.json({error: true});
				console.log('Fehler: DELETE / konnte Klasse nicht löschen');
				console.log(fehler);
			});
		})
	})
	return router;
}

module.exports = exp;