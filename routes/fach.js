const express = require('express');


const exp = function(db) {
	const router = express.Router();
	// liest Fächer aus Datenbank aus
    // Eingabewert: nichts
    // Ausgabewert: JSON: {error: true, grund: ...} oder [fächer]
	router.get('/', function(req, res) {
		var faecher = db.Fach.findAll().then(faecher => {
			return res.json(faecher);
		}).catch(err => {
			res.json({error: 'true', reason: 'Datenbank'});
			console.log('Fehler: Fächer /get konnte Fächer nicht aus Datenbank auslesen');
			console.log(err);
		})
	});
	// Erstellt neues Fach
    // Eingabewert: POST-Daten typ stufe_von stufe_bis bezeichnung kuerzel
    // Ausgabewert: JSON: {error: true} oder {error: false, id: erstelltesFachUUID}
	router.post('/', function(req, res) {
	
		//express speichert POST-daten in req.body
		const data = req.body;
		//extrahiere die Daten (unnötig aber lässt raum für weitere validierung)
		const typ = data.typ;
		const stufe_von = data.stufe_von;
		const stufe_bis = data.stufe_bis;
		const bezeichnung = data.bezeichnung;
		const kuerzel = data.kuerzel;

		const neuesFach = db.Fach.build({
			typ: typ,
			stufe_von: stufe_von,
			stufe_bis: stufe_bis,
			bezeichnung: bezeichnung,
			kuerzel: kuerzel
		});
		neuesFach.save().then(erstelltesObjekt => {
			//konnte abgespeichert werden
			res.json({error: false, id: erstelltesObjekt.uuid});
		}).catch(err => {
			res.json({error: true});
			console.log('Fehler: POST / konnte Fach nicht abspeichern');
			console.log(err);
		});
	});
	// aktualisiert Daten eines Faches (veränderte Bezeichnung, typ, stufe_von, stufe_bis, ...)
    // Eingabewert: URL-Daten: uuid | POST-Daten typ stufe_von stufe_bis bezeichnung kuerzel
    // Ausgabewert: JSON: {error: true} oder {error: false}
	router.put('/:uuid', function(req, res) {
		const data = req.body;
		db.Fach.findOne({where: {
			uuid: req.params.uuid}
		}).then(fach => {
			fach.update(data).then(_ => {
				res.json({error: false});
			});
		}).catch(fehler => {
			res.json({error: true});
			console.log('Fehler: PUT / konnte Fach nicht aktualisieren');
			console.log(fehler);
		});
	});
	// löscht Fach sofern kein Schüler mehr dieses Fach gewählt hat
    // Eingabewert: URL-Daten: uuid
    // Ausgabewert: JSON: {error: true, grund: ...} oder {error: false}
	router.delete('/:uuid', function(req, res) {
		db.Fach.findOne({where: {
			uuid: req.params.uuid}
		}).then(fach => {
			if(fach.typ !== 'normal') {
				const fach_key = fach.typ + "_id";
				const fach_uuid = fach.uuid;
				var query = {where: {}};
				query.where[fach_key] = fach_uuid;
				db.Schueler.findAll(query)
				.then(schueler => {
					if(schueler.length !== 0) {
						return res.json({error: true, grund: 'Fach noch Wahl einiger Schüler'});
					} else {
						fach.destroy().then(_ => {
							res.json({error: false});
						});
					}
				})
			} else {
				fach.destroy().then(_ => {
					res.json({error: false});
				});
			}
		}).catch(fehler => {
			res.json({error: true});
			console.log('Fehler: DELETE / konnte Fach nicht löschen');
			console.log(fehler);
		});
	})
	return router;
}

module.exports = exp;