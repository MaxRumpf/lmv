const express = require('express');


const exp = function(db) {
    const router = express.Router();
    // liest das Deputat zu einem Fach und zu einer Klasse aus
    // Eingabewert: URL-Daten: fach_id klasse_id
    // Ausgabewert: JSON: {error: true, reason: ...} oder {error: false, uuid: null/uuid, name: nameDesLehrers}
	router.get('/:fach_id/:klasse_id', function(req, res) {
		var deputate = db.Deputat.findAll({
            where: {
                fach_id: req.params.fach_id,
                klasse_id: req.params.klasse_id
            }
        }).then(gefundeneDeputate => {
            console.log(gefundeneDeputate);
            if(!gefundeneDeputate || gefundeneDeputate.length !== 1) {
                for(var i=0;i<gefundeneDeputate.length;i++) {
                    gefundeneDeputate[i].destroy();
                }
                return res.json({error: false, uuid: null});
            }
            db.Schueler.findAll({
                where: {
                    uuid: gefundeneDeputate[0].schueler_id
                }
            })
            .then(gefundeneSchueler => {
                console.log(gefundeneSchueler);
                if(gefundeneSchueler && gefundeneSchueler.length !== 1) {
                    return res.json({error: false, uuid: null});
                }
                return res.json({error: false, uuid: gefundeneSchueler[0].uuid, name: gefundeneSchueler[0].name});
            })
		}).catch(err => {
			res.json({error: true, reason: 'Datenbank'});
			console.log('Fehler: Deputate GET konnte Deputate nicht aus Datenbank auslesen');
			console.log(err);
		})
    });
    // verändert Deputat einer Klasse in einem Fach
    // Eingabewert: URL-Daten: fach_id klasse_id | POST-Daten schueler_id
    // Ausgabewert: JSON: {error: true/false}
	router.post('/:fach_id/:klasse_id', function(req, res) {
	
		//express speichert POST-daten in req.body
		const data = req.body;
		//extrahiere die Daten (unnötig aber lässt raum für weitere validierung)
		const fach_id = req.params.fach_id;
		const klasse_id = req.params.klasse_id;
		const schueler_id = data.schueler_id;

		db.Deputat.findAll({
            where: {
                fach_id: fach_id,
                klasse_id: klasse_id
            }
        })
        .then(schonVorhandeneDeputate => {
            if(schonVorhandeneDeputate && schonVorhandeneDeputate.length !== 0) {
                console.log(schonVorhandeneDeputate);
                //Deputat existiert schon, muss also verändert werden
                schonVorhandeneDeputate[0].update({schueler_id: schueler_id})
                .then(_ => {
                    res.json({error: false});
                    for(var i=1;i<schonVorhandeneDeputate.length;i++) {
                        schonVorhandeneDeputate[i].destroy();
                    }
                })
            } else {
                db.Deputat.build({
                    fach_id: fach_id,
                    klasse_id: klasse_id,
                    schueler_id: schueler_id
                })
                .save(_ => {
                    console.log('saved');
                    res.json({error: false});
                })
            }
		}).catch(err => {
			res.json({error: true});
			console.log('Fehler: POST konnte Deputat nicht abspeichern');
			console.log(err);
		});
    });
	return router;
}

module.exports = exp;