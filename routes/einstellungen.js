// Dies ist ein Einstellungs-Modul, welches im Moment nur für das Speichern der Anzahlen der neuen Schüler in der 5. Klasse,
// und der Anzahl der Evangelischen, Katholischen und Ethik-Schüler dabei
const express = require('express');

var db;

const EINSTELLUNGEN = ['neueSchueler', 'neueRESchueler', 'neueRKSchueler', 'neueETSchueler'];

// erstellt falls nicht existent einen Eintrag in der Einstellungen-Datenbank mit dem Schlüssel schlüssel
function frageSchlüsselAbUndErstelleFallsNichtExistiert(schlüssel) {
    return new Promise(function (resolve, reject) {
        db.Einstellung.findAll({
            where: {
                schluessel: schlüssel
            }
        })
        .then(gefundeneEinträge => {
            if(gefundeneEinträge.length === 0) {
                db.Einstellung.build({
                    schluessel: schlüssel,
                    wert: ''
                })
                .save(_ => {
                    resolve({schlüssel: schlüssel, wert: ''});
                })
                .catch(fehler => {
                    reject(fehler);
                })
            } else if (gefundeneEinträge.length === 1) {
                resolve({schlüssel: schlüssel, wert: gefundeneEinträge[0].wert});
            } else {
                resolve({schlüssel: schlüssel, wert: gefundeneEinträge[0].wert});
                for(var i=1;i<gefundeneEinträge.length;i++) {
                    gefundeneEinträge[i].destroy().then();
                }
            }
        })
    });
}

const exp = function(_db) {
    db = _db;
    const router = express.Router();
    // fragt Schlüssel ab und gibt sie in JSON Objekt zurück
    // Eingabewert: nichts
    // Ausgabewert: JSON: {error: true} oder {error: false, einstellungen: [{schlüssel: ..., wert: ...}, ...]}
	router.get('/', function(req, res) {
        var queries = [];
        for(var i=0;i<EINSTELLUNGEN.length;i++) {
            var schlüssel = EINSTELLUNGEN[i];
            queries.push(frageSchlüsselAbUndErstelleFallsNichtExistiert(schlüssel));
        }
        Promise.all(queries)
        .then(wertPaare => {
            console.log(wertPaare);
            res.json({error: false, einstellungen: wertPaare});
        })
        .catch(fehler => {
            console.log(fehler);
            res.json({error: true});
        })
    });
    // speichert Wert für Schlüssel ab
    // Eingabewert: POST-Daten schluessel wert
    // Ausgabewert: JSON: {error: true} oder {error: false}
    router.post('/', function(req, res) {
        const schlüssel = req.body.schluessel;
        const wert = req.body.wert;
        if(!schlüssel || !wert) {
            return res.json({error: true});
        }
        db.Einstellung.findOne({
            where: {
                schluessel: schlüssel
            }
        })
        .then(einstellung => {
            if(!einstellung) {
                return res.json({error: true});
            }
            einstellung.update({wert: wert})
            .then(_ => {
                return res.json({error: false});
            })
        })
        .catch(fehler => {
            console.log(fehler);
            return res.json({error: true});
        })
    })
	return router;
}

module.exports = exp;