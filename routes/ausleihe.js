/*
    Werbserver-Plugin wird importiert, um eine sogenanntes Router-Objekt zu erstellen.
    Dieses Router-Objekt ist ein 'unter-Server', welcher die Strukturierung des Projekts vereinfacht.
    Methoden in diesem Router-Objekt werden mit router.get(url, handlerFunktion)/router.post(...)/router.put(...)/router.delete(...) definiert
        (url: string, handlerFunktion = function(req, res))
        [siehe Express API Docs]
*/
const express = require('express');
//Da das Datenbank-Objekt verfügbar gemacht werden muss wird hier eine Funktion exportiert, die nach Eingabe eines Datenbank-Objektes den Router zurückgibt
const exp = function(db) {
    const router = express.Router();

    // Errechnet Anzahlen der jeweiligen Ausleihen aller Bücher
    // Eingabeparameter: nichts
    // Antwort: {error: true/false, data: [{'uuidString': AnzahlInteger}])
    router.get('/anzahlen', (req, res) => {
        db.Buch.findAll()
        .then(buecher => {
            var queries = [];
            for(var i=0;i<buecher.length;i++) {
                var _buch = buecher[i];
                var query = new Promise(function(resolve, reject) {
                    var __buch = _buch;
                    db.Ausleihe.findAll({where: {buch_id: __buch.uuid}})
                    .then(ausleihen => {
                        resolve({uuid: __buch.uuid, anzahl: ausleihen.length});
                    })
                    .catch(fehler => {
                        reject(fehler);
                    });
                });
                queries.push(query);
            }
            Promise.all(queries)
            .then(antworten => {
                var ergebnisse = {};
                for(var i=0;i<antworten.length;i++) {
                    var _antwort = antworten[i];
                    ergebnisse[_antwort.uuid] = _antwort.anzahl;
                }
                res.json({error: false, data: ergebnisse});
            })
            .catch(fehler => {
                res.json({error: true});
                console.log(req.url + ':Fehler');
                console.log(fehler);
            })
        });
    });

    // Gibt Liste der Schüler aus, die in Klasse :klasse sind, Fach :fach haben und sagt aus, ob sie Buch :buch ausgeliehen haben
    // Schüler die Buch nicht ausgeliehen haben werden also auch angezeit
    // Eingabewerte Typ:URL /liste/kfb/*klasse*/*fach*/*buch* [jeweils UUID der Klasse, des Faches, des Buches]
    // Eingabewerte Beispiel: /liste/kfb/01cdef21oihb2/iljih2uo42/lj24bi2421 
    // Ausgabewerte: [{schueler: *komplettesSchülerObjektAusDatenbank*, ausgeliehen: true/false}]
	router.get('/liste/kfb/:klasse/:fach/:buch', function(req, res) {
        const klasse_id = req.params.klasse;
        const fach_id = req.params.fach;
        const buch_id = req.params.buch;
        db.Schueler.findAll({
            where: {
                klasse_id: klasse_id
            }
        }).then(schuelerInKlasse => {
            db.Klasse.findOne({where: {uuid: klasse_id}})
            .then(klasse => {
                db.Fach.findOne({
                    where: {
                        uuid: fach_id
                    }
                }).then(fach => {
                    var zutreffendeSchueler = [];
                    if(fach.typ !== 'normal' && klasse.stufe !== 0) {
                        for(var i=0;i<schuelerInKlasse.length;i++) {
                            var _schueler = schuelerInKlasse[i];
                            if(_schueler[fach.typ + '_id'] === fach.uuid) {
                                zutreffendeSchueler.push(_schueler);
                            }
                        }
                    } else {
                        zutreffendeSchueler = schuelerInKlasse;
                    }
                    var queries = [];
                    for(var i=0;i<zutreffendeSchueler.length;i++) {
                        var _schueler = zutreffendeSchueler[i];
                        queries.push(
                            new Promise(function(resolve, reject) {
                                var __schueler = _schueler;
                                db.Ausleihe.findAll({
                                    where: {
                                        schueler_id: __schueler.uuid,
                                        buch_id: buch_id
                                    }
                                }).then(e => {
                                    if(e.length === 0) {
                                        resolve({
                                            schueler: __schueler,
                                            ausgeliehen: false
                                        });
                                    } else if (e.length === 1) {
                                        resolve({
                                            schueler: __schueler,
                                            ausgeliehen: true
                                        });
                                    } else {
                                        console.log('Schüler hat Buch mehrmals ausgeliehen!');
                                        resolve({
                                            schueler: __schueler,
                                            ausgeliehen: true
                                        });
                                    }
                                }).catch(e => {reject(fehler)});
                            })
                        );
                    }
                    var antwort = [];
                    Promise.all(queries).then(ergebnisse => {
                        for(var i=0;i<ergebnisse.length;i++) {
                            antwort.push(ergebnisse[i]);
                        }
                        res.json(antwort);
                    });
                })
            })
        }).catch(console.log.bind)
    });
    // Errechnet Anzahl der Ausleihen in einer Klasse
    // = Das gleiche wie /liste/kfb/... nur dass jeglich eine Anzahl ausgegeben wird
    // Eingabewerte in URL: /anzahl/kfb/*klasseUUID*/*fachUUID*/*buchUUID*
    //Ausgabewerte: {koennenAusleihen: könnenAusleihenAnzahl, habenAusgeliehen: habenAusgeliehenAnzahl}
    router.get('/anzahl/kfb/:klasse/:fach/:buch', function(req, res) {
        const klasse_id = req.params.klasse;
        const fach_id = req.params.fach;
        const buch_id = req.params.buch;
        db.Schueler.findAll({
            where: {
                klasse_id: klasse_id
            }
        }).then(schuelerInKlasse => {
            db.Klasse.findOne({where: {uuid: klasse_id}})
            .then(klasse => {
                db.Fach.findOne({
                    where: {
                        uuid: fach_id
                    }
                }).then(fach => {
                    var zutreffendeSchueler = [];
                    if(fach.typ !== 'normal' && klasse.stufe !== 0) {
                        for(var i=0;i<schuelerInKlasse.length;i++) {
                            var _schueler = schuelerInKlasse[i];
                            if(_schueler[fach.typ + '_id'] === fach.uuid) {
                                zutreffendeSchueler.push(_schueler);
                            }
                        }
                    } else {
                        zutreffendeSchueler = schuelerInKlasse;
                    }
                    var queries = [];
                    for(var i=0;i<zutreffendeSchueler.length;i++) {
                        var _schueler = zutreffendeSchueler[i];
                        queries.push(
                            new Promise(function(resolve, reject) {
                                var __schueler = _schueler;
                                db.Ausleihe.findAll({
                                    where: {
                                        schueler_id: __schueler.uuid,
                                        buch_id: buch_id
                                    }
                                }).then(e => {
                                    if(e.length === 0) {
                                        resolve({
                                            schueler: __schueler,
                                            ausgeliehen: false
                                        });
                                    } else if (e.length === 1) {
                                        resolve({
                                            schueler: __schueler,
                                            ausgeliehen: true
                                        });
                                    } else {
                                        console.log('Schüler hat Buch mehrmals ausgeliehen!');
                                        resolve({
                                            schueler: __schueler,
                                            ausgeliehen: true
                                        });
                                    }
                                }).catch(e => {reject(fehler)});
                            })
                        );
                    }
                    var antwort = [];
                    Promise.all(queries).then(ergebnisse => {
                        var könnenAusleihen = 0;
                        var habenAusgeliehen = 0;
                        for(var i=0;i<ergebnisse.length;i++) {
                            könnenAusleihen++;
                            if(ergebnisse[i].ausgeliehen) {
                                habenAusgeliehen++;
                            }
                        }
                        res.json({koennenAusleihen: könnenAusleihen, habenAusgeliehen: habenAusgeliehen});
                    });
                })
            })
        }).catch(console.log.bind)
    });

    //gibt Ausleihe-Liste eines Schülers aus
    //hierzu zählen alle Bücher die er ausgeliehen hat (ob überfällig oder nicht) und alle, die er ausleihen können sollte
    //Eingabewert Url: /liste/s/*schülerUUID*
    //Ausgabewert: [{buch: buchUUID, ausgeliehen: true/false}]
    router.get('/liste/s/:schueler', function(req, res) {
        const schueler_id = req.params.schueler;
        db.Schueler.findOne({where: {uuid: schueler_id}})
        .then(schueler => {
            db.Klasse.findOne({where: {uuid: schueler.klasse_id}})
            .then(klasse => {
                db.Fach.findAll()
                .then(alleFaecher => {
                    db.Buch.findAll()
                    .then(alleBuecher => {
                        db.Ausleihe.findAll({where: {schueler_id: schueler_id}})
                        .then(ausleihenDesSchuelers => {
                            var antwort = [];
                            var restlicheAusleihenDesSchuelers = ausleihenDesSchuelers.slice();
                            for(var a=0;a<alleFaecher.length;a++) {
                                var _fach = alleFaecher[a];
                                var schuelerHatFach = true;
                                if(_fach.typ != 'normal') {
                                    schuelerHatFach = (schueler[_fach.typ + '_id'] == _fach.uuid);
                                }
                                if(_fach.stufe_von > klasse.stufe || _fach.stufe_bis < klasse.stufe) {
                                    schuelerHatFach = false;
                                }
                                if(klasse.stufe != 0 && !schuelerHatFach) {
                                    continue;
                                }
                                //Schueler hat Fach
                                for(var b=0;b<alleBuecher.length;b++) {
                                    var _buch = alleBuecher[b];
                                    if(_buch.fach_id !== _fach.uuid) {
                                        continue;
                                    }
                                    if(klasse.stufe != 0 && (_buch.stufe_von > klasse.stufe || _buch.stufe_bis < klasse.stufe)) {
                                        continue;
                                    }
                                    //Schueler hat Buch bzw muss es bekommen
                                    //Hat er Buch schon ausgeliehen?
                                    var buchAusgeliehen = false;
                                    for(var c=0;c<ausleihenDesSchuelers.length;c++) {
                                        var _ausleihe = ausleihenDesSchuelers[c];
                                        console.log(restlicheAusleihenDesSchuelers.length);
                                        if(_ausleihe.buch_id === _buch.uuid) {
                                            restlicheAusleihenDesSchuelers = restlicheAusleihenDesSchuelers.filter(a => {console.log(a.uuid + ":" + _ausleihe.uuid, (a.uuid !== _ausleihe.uuid));return a.uuid !== _ausleihe.uuid});
                                            buchAusgeliehen = true;
                                            break;
                                        }
                                    }
                                    antwort.push({buch: _buch, ausgeliehen: buchAusgeliehen});
                                }
                            }
                            for(var i=0;i<restlicheAusleihenDesSchuelers.length;i++) {
                                var _aktuelleAusleihe = restlicheAusleihenDesSchuelers[i];
                                for(var x=0;x<alleBuecher.length;x++) {
                                    var _buch = alleBuecher[x];
                                    if(_buch.uuid === _aktuelleAusleihe.buch_id) {
                                        antwort.push({buch: _buch, ausgeliehen: true});
                                    }
                                }
                            }
                            res.json(antwort);
                        })
                    })
                })
            })
        })
        .catch(fehler => {
            res.json({error: true});
            console.log(fehler);
        })
    });
    //gibt Liste aller Schüler
    // Eingabewert URL: /liste/b/*buchUUID*
    // Ausgabewert: {error: false/true, data: [{schueler: schuelerDatenbankObjekt, ausgeliehen: true/false}]}
    router.get('/liste/b/:buch', function(req, res) {
        const buch_id = req.params.buch;
        db.Ausleihe.findAll({where: {buch_id: buch_id}})
        .then(ausleihen => {
            var queries = [];
            for(var i=0;i<ausleihen.length;i++) {
                var _ausleihe = ausleihen[i];
                var query = new Promise(function(resolve, reject) {
                    var __ausleihe = _ausleihe;
                    db.Schueler.findOne({where: {uuid: __ausleihe.schueler_id}})
                    .then(schueler => {
                        if(schueler) {
                            resolve({schueler: schueler, ausgeliehen: true});
                        } else {
                            console.log('/liste/b/:buch: Schüler nicht gefunden, wird ignoriert.');
                            resolve();
                        }
                    })
                    .catch(fehler => {
                        reject(fehler);
                    })
                })
                queries.push(query);
            }
            Promise.all(queries)
            .then(objekte => {
                res.json({error: false, data: objekte.filter(o => o !== undefined)});
            })
            .catch(fehler => {
                res.json({error: true});
                console.log(req.url + ': ERROR');
                console.log(fehler);
            })

        })
        .catch(fehler => {
            res.json({error: true});
            console.log(fehler);
        })
    });
    // speichert Ausleihe ab -> Schüler leiht Buch aus
    // Eingabewert: POST-Parameter schueler_id buch_id
    // Ausgabewert: JSON: {error: true/false}
	router.post('/', function(req, res) {
        
		//express speichert POST-daten in req.body
		const data = req.body;
		//extrahiere die Daten (unnötig aber lässt raum für weitere validierung)
        const schueler_id = data.schueler_id;
        const buch_id = data.buch_id;
        
		const ausleihe = db.Ausleihe.build({
            schueler_id: schueler_id,
            buch_id: buch_id
		});
		ausleihe.save().then(_ => {
			//konnte abgespeichert werden
			res.json({error: false});
		}).catch(err => {
			res.json({error: true});
			console.log('Fehler: POST / konnte Ausleihe nicht abspeichern');
			console.log(err);
		});
    });
    // löscht alle Ausleihe-Objekte -> Schüler leiht Buch nichtmehr aus
    // um Duplikate einer Ausleihe zu vermeiden werden alle Ausleihen mit schueler_id == schueler_id
    // und buch_id == buch_id gelöscht, obwohl pro Schüler und Buch maximal eine existieren sollte
    // Eingabewert: URL-Parameter schueler_id buch_id
    // Ausgabewert: JSON: {error: true/false}
	router.delete('/:schueler_id/:buch_id', function(req, res) {
        //bei HTTP DELETE gibts leider keinen body :/
        const data = req.params;
        const schueler_id = data.schueler_id;
        const buch_id = data.buch_id;
		db.Ausleihe.findAll({
            where: {
			    schueler_id: schueler_id,
                buch_id: buch_id
            }
		}).then(ausleihen => {
            var queries = [];
            for(var i=0;i<ausleihen.length;i++) {
                queries.push(ausleihen[i].destroy());
            }
            Promise.all(queries).then(_ => {
                res.json({error: false});
            })
		}).catch(fehler => {
			res.json({error: true});
			console.log('Fehler: DELETE / konnte Buch nicht löschen');
			console.log(fehler);
		});
	})
	return router;
}

module.exports = exp;