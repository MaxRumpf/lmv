// siehe ausleihe.js
const express = require('express');

var db;

const exp = function(_db) {
	db = _db;
	const router = express.Router();
	// liest alle Bücher aus Datenbank aus
    // Eingabewert: nichts
    // Ausgabewert: JSON: {error: true} oder [{buch1}, {buch2}, ...] Bücher hier jedoch ohne Ausleihen
	router.get('/', function(req, res) {
		var buecher = db.Buch.findAll().then(buecher => {
			return res.json(buecher);
		}).catch(err => {
			res.json({error: true, reason: 'Datenbank'});
			console.log('Fehler: Bücher /get konnte Bücher nicht aus Datenbank auslesen');
			console.log(err);
		})
	});
	// speichert neues Buch in Datenbank ab
    // Eingabewert: POST-Daten titel velag preis(Anmerkung: dezimalzahlen notiert in amerikanischer Schreibweise) stufe_von stufe_bis isbn bestand fach_id
    // Ausgabewert: JSON: {error: true} oder {error: false, id: erstelltesBuchUUID}
	router.post('/', function(req, res) {
	
		//express speichert POST-daten in req.body
		const data = req.body;
		//extrahiere die Daten (unnötig aber lässt raum für weitere validierung)
		const titel = data.titel;
		const verlag = data.verlag;
		const preis = data.preis;
		const stufe_von = data.stufe_von;
		const stufe_bis = data.stufe_bis;
		const isbn = data.isbn;
		const bestand = data.bestand;
		const fach_id = data.fach_id;

		const neuesBuch = db.Buch.build({
			titel: titel,
			verlag: verlag,
			preis: preis,
			stufe_von: stufe_von,
			stufe_bis: stufe_bis,
			isbn: isbn,
			bestand: bestand,
			fach_id: fach_id,
			eingabedatum: Date.now()
		});
		neuesBuch.save().then(erstelltesObjekt => {
			//konnte abgespeichert werden
			res.json({error: false, id: erstelltesObjekt.uuid});
		}).catch(err => {
			res.json({error: true});
			console.log('Fehler: POST / konnte Buch nicht abspeichern');
			console.log(err);
		});
	});
	// aktualisiert Daten eines Buches (veränderter Titel, Verlag, Preis, ...)
    // Eingabewert: URL-Daten: uuid | POST-Daten titel velag preis(Anmerkung: dezimalzahlen notiert in amerikanischer Schreibweise) stufe_von stufe_bis isbn bestand fach_id
    // Ausgabewert: JSON: {error: true} oder {error: false}
	router.put('/:uuid', function(req, res) {
		const data = req.body;
		db.Buch.findOne({where: {
			uuid: req.params.uuid}
		}).then(buch => {
			buch.update(data).then(_ => {
				res.json({error: false});
			});
		}).catch(fehler => {
			res.json({error: true});
			console.log('Fehler: PUT / konnte Buch nicht aktualisieren');
			console.log(fehler);
		});
	});
	// löscht Buch aus Datenbank
    // Eingabewert: URL-Daten uuid
    // Ausgabewert: JSON: {error: true} oder {error: false}
	router.delete('/:uuid', function(req, res) {
		db.Buch.findOne({where: {
			uuid: req.params.uuid}
		}).then(buch => {
			buch.destroy().then(_ => {
				res.json({error: false});
			});
		}).catch(fehler => {
			res.json({error: true});
			console.log('Fehler: DELETE / konnte Buch nicht löschen');
			console.log(fehler);
		});
	})
	// rechnet die benötigte Anzahl an Bücher für das neue Schuljahr aus - Achtung: sehr komplex
    // Eingabewert: URL-Daten: fach_id
    // Ausgabewert: JSON: {error: true} oder {error: false, daten: [{buch: buchObjektAusDatenbank, anzahlRuecklaeufig: Zahl, anzahlBenoetigt: Zahl, anzahlAusgeliehen: Zahl}, ...]}
	router.get('/bestellungen/:fach_id', function(req, res) {
		//DATEN: buch, anzahlRückläufig, anzahlBenötigt
		db.Buch.findAll({
			where: {
				fach_id: req.params.fach_id
			}
		})
		.then(alleBücher => {
			var queries = [];
			alleBücher.forEach(buch => {
				queries.push(errechneBestellungsDatenFürBuch(buch));
			});
			Promise.all(queries)
			.then(daten => {
				res.json({error: false, daten: daten});
			})
			.catch(fehler => {
				res.json({error: true});
				console.log(fehler);
			})
		})
		.catch(fehler => {
			console.log(fehler);
			res.json({error: true});
		})
	});
	return router;
}

//Nun folgen die Funktionen zur Berechnung der benötigten Anzahl an Büchern, der rückläfigen Anzahl, etc

function alleAusleihenDesBuches(buch) {
	//Datenformat: [{schueler: db.Schueler, klasse: db.Klasse}]
	return new Promise((resolve, reject) => {
		db.Ausleihe.findAll({
			where: {
				buch_id: buch.uuid
			}
		})
		.then(alleAusleihenDesBuches => {
			var queries = [];
			alleAusleihenDesBuches.forEach(ausleihe => {
				queries.push(new Promise((res1, rej1) => {
					db.Schueler.findOne({
						where: {
							uuid: ausleihe.schueler_id
						}
					})
					.then(s => {
						db.Klasse.findOne({where: {uuid: s.klasse_id}})
						.then(k => {
							res1({schüler: s, klasse: k});
						})
					})
					.catch(fehler => {
						rej1(fehler);
					})
				}));
			});
			Promise.all(queries)
			.then(ausleiheObjekte => {
				resolve(ausleiheObjekte);
			})
		})
	});
}

function schülerBenotigtBuch(buch, schüler) {
	return new Promise((resolve, reject) => {
		db.Klasse.findOne(
			{where: {uuid: schüler.klasse_id}}
		)
		.then(klasse => {
			if(klasse.stufe === 0) {
				return resolve(false);
			}
			db.Fach.findOne({
				where: {
					uuid: buch.fach_id
				}
			})
			.then(buchFach => {
				var buchFürSchüler = true;
				if(buchFach.typ !== 'normal') {
					buchFürSchüler = (schüler[buchFach.typ + '_id'] === buchFach.uuid);
				}
				buchFürSchüler = buchFürSchüler && buch.stufe_von <= klasse.stufe + 1;
				buchFürSchüler = buchFürSchüler && buch.stufe_bis >= klasse.stufe + 1;
				resolve(buchFürSchüler);
			})
		})
	});
}

function errechneBenötigtAnzahlFürBuch(buch) {
	return new Promise((resolve, reject) => {
		db.Schueler.findAll()
		.then(alleSchüler => {
			var queries = [];
			alleSchüler.forEach(schüler => {
				queries.push(schülerBenotigtBuch(buch, schüler));
			});
			Promise.all(queries)
			.then(antworten => {
				var anzahl = 0;
				antworten.forEach(antwort => {
					if(antwort) {
						anzahl++;
					}
				})
				console.log(buch.titel, anzahl);
				resolve(anzahl);
			})
		});
	});
}

function errechneBestellungsDatenFürBuch(buch) {
	return new Promise((resolve, reject) => {

		var anzahlRückläufig = 0;
		var anzahlBenötigt = 0;
		var anzahlAusgeliehen = 0;

		var stufeBis = buch.stufe_bis;
		alleAusleihenDesBuches(buch)
		.then(alleAusleihenDesBuches => {
			alleAusleihenDesBuches.forEach(ausleihe => {
				if(ausleihe.klasse.stufe !== 0 && ausleihe.klasse.stufe + 1 > stufeBis) {
					anzahlRückläufig++;
				} else {
					anzahlAusgeliehen++;
				}
				console.log(anzahlRückläufig, " ", anzahlAusgeliehen);
			});
			console.log('alleAusleihenDesBuches fertig');
			errechneBenötigtAnzahlFürBuch(buch)
			.then(benötigteAnzahl => {
				anzahlBenötigt = benötigteAnzahl;
				if(buch.stufe_von === 5) {
					db.Fach.findOne({where: {uuid: buch.fach_id}})
					.then(fach => {
						if(fach.typ === "normal") {
							db.Einstellung.findOne({where: {schluessel: 'neueSchueler'}})
							.then(einstellung => {
								anzahlBenötigt = anzahlBenötigt + (+einstellung.wert);
								resolve({buch: buch, anzahlRuecklaeufig: anzahlRückläufig, anzahlBenoetigt: anzahlBenötigt, anzahlAusgeliehen: anzahlAusgeliehen});
							});
						} else if (fach.typ === 'konfession') {
							var schluessel = 'neue' + fach.kuerzel + 'Schueler';
							console.log(schluessel);
							db.Einstellung.findOne({where: {schluessel: schluessel}})
							.then(einstellung => {
								anzahlBenötigt = anzahlBenötigt + (+einstellung.wert);
								resolve({buch: buch, anzahlRuecklaeufig: anzahlRückläufig, anzahlBenoetigt: anzahlBenötigt, anzahlAusgeliehen: anzahlAusgeliehen});
							});
						} else {
							resolve({buch: buch, anzahlRuecklaeufig: anzahlRückläufig, anzahlBenoetigt: anzahlBenötigt, anzahlAusgeliehen: anzahlAusgeliehen});
						}
					})
				} else {
					resolve({buch: buch, anzahlRuecklaeufig: anzahlRückläufig, anzahlBenoetigt: anzahlBenötigt, anzahlAusgeliehen: anzahlAusgeliehen});
				}
			})
		})
	});
}

module.exports = exp;