const express = require('express');


const exp = function(db) {
	const router = express.Router();
	// liest alle Schüler aus
    // Eingabewert: nichts
    // Ausgabewert: JSON: {error: true} oder [schüler1, schüler2, ...]
	router.get('/', function(req, res) {
		var schueler = db.Schueler.findAll().then(schueler => {
			res.json(schueler);
		}).catch(err => {
			res.json({error: 'true', reason: 'Datenbank'});
			console.log('Fehler: Schüler /get konnte Schüler nicht aus Datenbank auslesen');
			console.log(err);
		});
	});
	// liest alle Lehrer aus (Lehrer sind in Klasse 'L')
    // Eingabewert: nichts
    // Ausgabewert: JSON: {error: true} oder {error: false, lehrer: [lehrer1, lehrer2, ...]}
	router.get('/lehrer', function(req, res) {
		db.Klasse.findAll({
			where: {
				bezeichnung: 'L'
			}
		})
		.then(gefundeneKlassen => {
			if(!gefundeneKlassen || gefundeneKlassen.length !== 1) {
				return res.json({error: true, grund: 'Lehrer-Klasse existiert nicht'});
			}
			const lehrerKlasse = gefundeneKlassen[0];
			db.Schueler.findAll({
				where: {
					klasse_id: lehrerKlasse.uuid
				}
			})
			.then(alleLehrer => {
				res.json({error: false, lehrer: alleLehrer});
			})
		})
		.catch(fehler => {
			res.json({error: true, grund: 'sonstiges'});
			console.log(fehler);
		})
	});
	// liest alle Schüler in Klasse aus
    // Eingabewert: URL-Parameter klasseid
    // Ausgabewert: JSON: {error: true} oder {error: false, data: [schüler1, schüler2, ...]}
	router.get('/:klasseid', (req, res) => {
		const klasseID = req.params.klasseid;
		var queryObjekt = {};
		if(klasseID !== '*') {
			queryObjekt = {
				where: {
					klasse_id: klasseID
				}
			}
		}
		db.Schueler.findAll(queryObjekt).then(schuelerInKlasse => {
			res.json({error: false, data: schuelerInKlasse});
		}).catch(err => {
			res.json({error: true, reason: 'Datenbank'});
			console.log('Fehler: Schüler GET /:klasseid konnte Daten nicht aus Datenbank auslesen');
			console.log(err);
		});
	});
	// erstellt neues Schüler-Objekt
    // Eingabewert: POST-Parameter name geburtsdatum profil_id konfession_id fremdsprache_id klasse_id
    // Ausgabewert: JSON: {error: true} oder {error: false, id: erstellterSchülerUUID}
	router.post('/', function(req, res) {
		//express speichert POST-daten in req.body
		const data = req.body;
		//extrahiere die Daten (unnötig aber lässt raum für weitere validierung)
		const name = data.name;
		const geburtsdatum = data.geburtsdatum;
		const profil_id = data.profil_id;
		const konfession_id = data.konfession_id;
		const fremdsprache_id = data.fremdsprache_id;
		const klasse_id = data.klasse_id;
		
		const neuerSchueler = db.Schueler.build({
			name: name,
			geburtsdatum: geburtsdatum,
			profil_id: profil_id,
			konfession_id: konfession_id,
			fremdsprache_id: fremdsprache_id,
			klasse_id: klasse_id
		});
		neuerSchueler.save().then(erstelltesObjekt => {
			//konnte abgespeichert werden
			res.json({error: false, id: erstelltesObjekt.uuid});
		}).catch(err => {
			res.json({error: true});
			console.log('Fehler: POST / konnte Schüler nicht abspeichern');
			console.log(err);
		});
	});
	// aktualisiert Daten eines Schülers
    // Eingabewert: URL-Parameter uuid | POST-Parameter name geburtsdatum profil_id konfession_id fremdsprache_id klasse_id
    // Ausgabewert: JSON: {error: true} oder {error: false}
	router.put('/:uuid', function(req, res) {
		const data = req.body;
		db.Schueler.findOne({where: {
			uuid: req.params.uuid}
		}).then(schueler => {
			schueler.update(data).then(_ => {
				res.json({error: false});
			});
		}).catch(fehler => {
			res.json({error: true});
			console.log('Fehler: PUT / konnte Schüler nicht aktualisieren');
			console.log(fehler);
		});
	});
	// verschiebt Schüler in Klasse 'A' oder löscht ihn sofern er schon in 'A' ist
    // Eingabewert: URL-Parameter uuid
    // Ausgabewert: JSON: {error: true} oder {error: false}
	router.delete('/:uuid', function(req, res) {
		const uuid = req.params.uuid;
		db.Schueler.findOne({where: {
			uuid: uuid}
		}).then(schueler => {
			db.Klasse.findOne({where: {
				uuid: schueler.klasse_id
			}})
			.then(klasse => {
				if(klasse.stufe === 0) {
					schueler.destroy().then(_ => {
						res.json({error: false});
					});
				} else {
					db.Klasse.findOne({where: {
						stufe: 0,
						bezeichnung: 'A'
					}})
					.then(abgängerKlasse => {
						if(abgängerKlasse) {
							schueler.update({
								klasse_id: abgängerKlasse.uuid
							})
							.then(_ => {
								res.json({error: false});
							})
						} else {
							schueler.destroy().then(_ => {
								res.json({error: false});
							});
						}
					})
				}
			})
		}).catch(fehler => {
			res.json({error: true});
			console.log('Fehler: DELETE / konnte Schüler nicht löschen');
			console.log(fehler);
		});
	});
	// Verschiebt Schüler in neue Klasse
    // Eingabewert: URL-Parameter uuid | POST-Parameter schuelerUUID neueKlasseUUID
    // Ausgabewert: JSON: {error: true} oder {error: false}
	router.post('/verschieben/:uuid', function(req, res) {
		const schuelerUUID = req.params.uuid;
		const neueKlasseUUID = req.body.neueKlasse;
		if(!schuelerUUID) {
			return res.json({error: true, grund: 'leere Schüler-UUID'});
		}
		db.Schueler.findOne({
			where: {
				uuid: schuelerUUID
			}
		})
		.then(schueler => {
			if(!schueler) {
				return res.json({error: true, grund: 'Schüler nicht gefunden'});
			}
			if(schueler.klasse_id === neueKlasseUUID) {
				return res.json({error: true, grund: 'Schüler schon in Klasse'});
			}
			db.Klasse.findOne({
				where: {
					uuid: neueKlasseUUID
				}
			})
			.then(neueKlasseObjekt => {
				if(!neueKlasseObjekt) {
					return res.json({error: true, grund: 'neue Klasse nicht gefunden'});
				}
				schueler.update({klasse_id: neueKlasseObjekt.uuid})
				.then(_ => {
					res.json({error: false});
				})
				.catch(_ => {
					res.json({error: true, grund: 'Klasse konnte nicht geändert werden'});
				})
			})
			.catch(_ => {
				res.json({error: true, grund: 'neue Klasse nicht gefunden'});
			})
		})
		.catch(_ => {
			res.json({error: true, grund: 'Schüler nicht gefunden'});
		})
	})
	return router;
}

module.exports = exp;