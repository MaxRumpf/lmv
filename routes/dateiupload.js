// Diese Datei ist ein Handler für das Hochladen und Importieren der .dat- und .xls-Dateien
// Hier wird ledeglich das Hochladen und das Detektieren des Dateityps anhand der Dateinamen gehandelt
const express = require('express');
const multer = require('multer');

const fs = require('fs');
const path = require('path');

var db;

// Hier werden die einzelnen Module für den Import der .dat- / .xls-Dateien importiert
const buecherDatParser = require('../parsers/buecherdat.js');
const ausleihDatParser = require('../parsers/ausleihdat.js');
const schuelerCsvParser = require('../parsers/schueler.js');

// Multer ist ein Plugin für das Hochladen von Dateien über den Browser
var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './import_dateien/uploaded');
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname + '-' + Date.now());
    }
});
var upload = multer({ storage : storage }).array('file',10);

// Es folgen zwei ähnliche Versionen einer Funktion, die die Dateien die vorher hochgeladen wurden anhand des Dateiname
// zu ihrem Typ zuordnet und dem Modul, welches für das Importieren der Datei zuständig ist übergibt
// Bei analysiereDatei werden die Daten jeglich ausgewertet und gezählt, wie viele tatsächlich zur Datenbank hinzugefügt werden
// würden (da sie keine Duplikate sind, da verknüpfte Fächer etc existieren, ...). Dies ist eine Möglichkeit für den Benutzer
// vorher zu prüfen, welche Auswirkung das Importieren der Datei hätte.
// Bei importiereDatei passiert das gleiche, nur dass die Datei auch direkt importiert wird.
var analysiereDatei = function(req, res) {
    const file = req.files[0];
    const originalerDateiName = file.originalname;
    const filePath = file.path;
    let handler;
    let dateiTyp;
    switch(originalerDateiName) {
        case 'buecher.dat':
        handler = buecherDatParser.parseFile(filePath, db);
        dateiTyp = 'buecher.dat Datei';
        break;
        case 'ausleih.dat':
        handler = ausleihDatParser.parseFile(filePath, db);
        dateiTyp = 'ausleih.dat Datei';
        break;
        case 'schueler.xls':
        handler = schuelerCsvParser.parseFile(filePath, db);
        dateiTyp = 'schüler.xls Datei';
        break;
        default: handler = null; break;
    }
    if(!handler) {
        //Dateiname passt nicht zu bekannten Dateitypen.
        res.json({error: true, grund: 'filetype'});
    } else {
        console.log(handler);
        handler
        .then(antwort => {
            const datenAnzahl = antwort.valideObjekte.length;
            const parserFehler = antwort.parserFehler;
            const datenbankFehler = antwort.datenbankFehler;
            res.json({error: false, dateiTyp: dateiTyp, datei: file.filename, datenAnzahl: datenAnzahl, parserFehler: parserFehler, datenbankFehler: datenbankFehler});
        }, fehler => {
            if(fehler && typeof fehler !== 'string') {
                //wandle Fehler in Text um, sofern er das nicht ist
                fehler = JSON.stringify(fehler);
            }
            res.json({error: true, grund: fehler ? fehler : 'Fehler beim Lesen der Datei'});
        });
    }
}

var importiereDatei = function(req, res) {
    const dateiName = req.params.datei;
    if(!dateiName) {
        return res.json({error: true, grund: "Dateiname leer"})
    }
    const dateiPfad = path.join(__dirname, '..', 'import_dateien', 'uploaded', dateiName);
    console.log(dateiPfad);
    let handler;
    if (dateiName.startsWith('buecher.dat')) {
        handler = buecherDatParser.parseFile(dateiPfad, db, true);
    } else if (dateiName.startsWith('ausleih.dat')) {
        handler = ausleihDatParser.parseFile(dateiPfad, db, true);
    } else if (dateiName.startsWith('schueler.xls')) {
        handler = schuelerCsvParser.parseFile(dateiPfad, db, true);
    } else {
        return res.json({error: true, grund: 'Unbekannter Dateityp'});
    }
	console.log('Starte Import');
    handler
    .then(antwort => {
	console.log('Import fertig');
        res.json({error: false});
    }, fehler => {
        console.log(fehler);
        if(fehler && typeof fehler !== 'string') {
            //wandle Fehler in Text um, sofern er das nicht ist
            fehler = JSON.stringify(fehler);
        }
        res.json({error: true, grund: fehler ? fehler : 'Fehler beim Importieren'});
    });
}

const exp = function(_db) {
    db = _db;
    const router = express.Router();
    // Dateiimport-Handler
    // siehe Multer API-Docs
	router.post('/upload', function(req, res) {
        upload(req, res, function(err) {
            if(err || req.files.length != 1) {
                //Fehler beim empfangen/abspeichern
                //oder Datei-Anzahl nicht 1
                console.log('Empfangene Datei konnte nicht empfangen/abgespeichert werden.');
                res.json({error: true, grund: 'Datei konnte nicht empfangen/abgespeichert werden'});
            } else {
                //kein Fehler beim empfangen/abspreichern
                //Es ist genau eine Datei hochgeladen worden
                //Dateityp wird nun geprüft, Daten evaluiert und verfügbar gemacht.
                analysiereDatei(req, res);
            }
        });
    });
    router.post('/import/:datei', (req, res) => {
        console.log('abc');
        importiereDatei(req, res);
    });
	return router;
}

module.exports = exp;