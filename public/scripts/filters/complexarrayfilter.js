(function() {
	'use strict';
	angular
    .module('app')
    .filter("complexArrayFilter", function() {
        return function(items, property, value) {
            var filteredItems = [];
            for(var i=0;i<items.length;i++) {
                var item = items[i];
                if (item.hasOwnProperty(property) && item[property].includes(value)) {
                    filteredItems.push(item);
                }
            }
            return filteredItems;
        }
    })
})();