/**
 * @ngdoc function
 * @name app.config:uiRouter
 * @description
 * # Config
 * Config for the router
 */
(function() {
    'use strict';
    angular
      .module('app')
      .run(runBlock)
      .config(config);

      runBlock.$inject = ['$rootScope', '$state', '$stateParams'];
      function runBlock($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }

      config.$inject =  ['$stateProvider', '$urlRouterProvider', 'MODULE_CONFIG'];
      function config( $stateProvider,   $urlRouterProvider,   MODULE_CONFIG ) {
        
        var p = getParams('layout'),
            l = p ? p + '.' : '',
            layout = '../views/layout/layout.html',
            dashboard = '../views/dashboard/dashboard.html';

        $urlRouterProvider
          .otherwise('/lmv/übersicht');
        $stateProvider
          .state('lmv', {
            abstract: true,
            url: '/lmv',
            views: {
              '': {
                templateUrl: layout
              }
            }
          })
            .state('lmv.übersicht', {
              url: '/übersicht',
              templateUrl: '../views/dashboard/startseite.html',
              data : { title: 'Übersicht' },
              controller: 'StartseiteCtrl',
              resolve: load(['scripts/controllers/startseite.controller.js'])
            })
            .state('lmv.ausleihe', {
              abstract: true,
              url: '/ausleihe',
              views: {
                '': {
                  templateUrl: '../views/layout/empty.html'
                }
              },
              resolve: load(['scripts/directives/ausleihe-auswahl.js'])
            })
            .state('lmv.ausleihe.klassen', {
              url: '/klassen',
              templateUrl: '../views/ausleihe/nachklassen.html',
              data : { title: 'Ausleihe-Liste'},
              controller: 'AusleiheKlassenCtrl',
              resolve: load(['assets/jspdf/jspdf.min.js','assets/jspdf/jspdf.plugin.autotable.min.js', 'scripts/services/ausleihe_klassen.service.js', 'scripts/controllers/ausleihe/ausleihe_klassen.controller.js'])
            })
            .state('lmv.ausleihe.schüler', {
              url: '/schüler?klasse?schueler',
              templateUrl: '../views/ausleihe/nachschuelern.html',
              data : { title: 'Ausleihe-Liste'},
              controller: 'AusleiheSchuelerCtrl',
              resolve: load(['assets/jspdf/jspdf.min.js','assets/jspdf/jspdf.plugin.autotable.min.js', 'mgcrea.ngStrap', 'scripts/filters/complexarrayfilter.js', 'scripts/services/ausleihe_schueler.service.js', 'scripts/controllers/ausleihe/ausleihe_schueler.controller.js']),
              reloadOnSearch: false
            })
            .state('lmv.ausleihe.bücher', {
              url: '/bücher',
              templateUrl: '../views/ausleihe/nachbuechern.html',
              data : { title: 'Ausleihe-Liste'},
              controller: 'AusleiheBuecherCtrl',
              resolve: load(['scripts/services/ausleihe_buecher.service.js', 'scripts/controllers/ausleihe/ausleihe_buecher.controller.js'])
            })
            // .state('lmv.ausleihe.fächer', {
            //   url: '/fächer',
            //   templateUrl: '../views/ausleihe/nachfaechern.html',
            //   data : { title: 'Ausleihe-Liste'},
            //   controller: 'AusleiheFaecherCtrl',
            //   resolve: load(['scripts/services/ausleihe_faecher.service.js', 'scripts/controllers/ausleihe_faecher.controller.js'])
            // })
            .state('lmv.stammdaten', {
              abstract: true,
              url: '/stammdaten',
              views: {
                '': {
                  templateUrl: '../views/layout/empty.html'
                }
              }
            })
            .state('lmv.stammdaten.klassen', {
              url: '/klassen',
              templateUrl: '../views/stammdaten/klassen.html',
              data: {title: 'Klassen'},
              controller: 'StammdatenKlassenCtrl',
              resolve: load(['scripts/services/klassen.service.js', 'scripts/controllers/stammdaten/klassen.controller.js'])
            })
            .state('lmv.stammdaten.faecher', {
              url: '/faecher',
              templateUrl: '../views/stammdaten/faecher.html',
              data: {title: 'Fächer'},
              controller: 'StammdatenFaecherCtrl',
              resolve: load(['scripts/services/faecher.service.js', 'scripts/controllers/stammdaten/faecher.controller.js'])
            })
            .state('lmv.stammdaten.schueler', {
              url: '/schueler?klasse',
              templateUrl: '../views/stammdaten/schueler.html',
              data: {title: 'Schüler'},
              controller: 'StammdatenSchuelerCtrl',
              resolve: load(['ui.bootstrap', 'scripts/services/date.js', 'scripts/services/schueler.service.js', 'scripts/controllers/stammdaten/schueler.controller.js', 'scripts/controllers/stammdaten/schueler_verschieben.modal.controller.js']),
              reloadOnSearch: false
            })
            .state('lmv.stammdaten.buecher', {
              url: '/buecher',
              templateUrl: '../views/stammdaten/buecher.html',
              data: {title: 'Bücher'},
              controller: 'StammdatenBuecherCtrl',
              resolve: load(['scripts/services/date.js', 'scripts/services/nums.js', 'scripts/services/buecher.service.js', 'scripts/controllers/stammdaten/buecher.controller.js'])
            })
            .state('lmv.stammdaten.deputate', {
              url: '/deputate',
              templateUrl: '../views/stammdaten/deputate.html',
              data: {title: 'Deputate'},
              controller: 'StammdatenDeputateCtrl',
              resolve: load(['scripts/services/multihttp.js', 'scripts/controllers/stammdaten/deputate.controller.js'])
            })
            .state('lmv.bestellungen', {
              url: '/bestellungen',
              templateUrl: '../views/bestellungen/uebersicht.html',
              data: {title: 'Bestellungen'},
              controller: 'BestellungenÜbersichtCtrl',
              resolve: load(['scripts/services/multihttp.js', 'scripts/services/date.js', 'scripts/services/nums.js', '../views/bestellungen/uebersicht.controller.js'])
            })
            .state('lmv.einstellungen', {
              abstract: true,
              url: '/einstellungen',
              views: {
                '': {
                  templateUrl: '../views/layout/empty.html'
                }
              }
            })
            .state('lmv.einstellungen.datenimport', {
              url: '/daten-import',
              templateUrl: '../views/einstellungen/datenimport.html',
              data: {title: 'Daten-Import'},
              controller: 'EinstellungenDatenImportCtrl',
              resolve: load(['angularFileUpload', 'ui.bootstrap', 'scripts/controllers/datenimport.modal.controller.js', 'scripts/controllers/datenimport.controller.js'])
            })
          .state('404', {
            url: '/404',
            templateUrl: '../views/misc/404.html'
          });


        function load(srcs, callback) {
          return {
              deps: ['$ocLazyLoad', '$q',
                function( $ocLazyLoad, $q ){
                  var deferred = $q.defer();
                  var promise  = false;
                  srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                  if(!promise){
                    promise = deferred.promise;
                  }
                  angular.forEach(srcs, function(src) {
                    promise = promise.then( function(){
                      angular.forEach(MODULE_CONFIG, function(module) {
                        if( module.name == src){
                          src = module.module ? module.name : module.files;
                        }
                      });
                      return $ocLazyLoad.load(src);
                    } );
                  });
                  deferred.resolve();
                  return callback ? promise.then(function(){ return callback(); }) : promise;
              }]
          }
        }

        function getParams(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
      }
})();
