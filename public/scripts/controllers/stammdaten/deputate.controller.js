(function() {
    'use strict';
    angular
        .module('app')
        .controller('StammdatenDeputateCtrl', StammdatenDeputateCtrl);
        StammdatenDeputateCtrl.$inject = ['$scope', '$http', 'MultiHTTP'];
        function StammdatenDeputateCtrl($scope, $http, MultiHTTP) {
            $scope._data = {};
            $scope._data.klassen = [];
            $scope._data.faecher = [];
            $scope._data.lehrer = [];

            $scope._anzeige = {};
            $scope._anzeige.klasse = [];
            $scope._anzeige.lehrer_id = '';

            $scope._auswahl = {};
            $scope._auswahl.fach = {};
            $scope._auswahl.klasse = {};

            $scope.setzeAnzeige = function() {
                $scope._anzeige.klassen = [];
                if($scope._auswahl.fach.uuid) {
                    for(var i=0;i<$scope._data.klassen.length;i++) {
                        var _klasse = $scope._data.klassen[i];
                        if($scope._auswahl.fach.stufe_von <= _klasse.stufe && $scope._auswahl.fach.stufe_bis >= _klasse.stufe) {
                            //Fach wird in ausgewählter Klasse unterrichtet
                            $scope._anzeige.klassen.push(_klasse);
                        }
                    }
                }
                $scope.aktualisiereDaten();
            }

            $scope.$watch('_auswahl', _ => {
                $scope.setzeAnzeige();
            }, true);

            $scope.setzeAuswahl = function(typ, objekt) {
                switch(typ) {
                    case 'klasse':
                        $scope._auswahl.klasse = objekt;
                        break;
                    case 'fach':
                        $scope._auswahl.fach = objekt;
                        break;
                    default:
                        console.log('$scope.setzeAuswahl: unbekannter typ');
                }
            }

            $scope.istAuswahl = function(typ, objekt) {
                switch(typ) {
                    case 'klasse':
                        return $scope._auswahl.klasse.uuid === objekt.uuid;
                        break;
                    case 'fach':
                        return $scope._auswahl.fach.uuid === objekt.uuid;
                        break;
                    default:
                        console.log('$scope.istAuswahl: unbekannter typ');
                }
            }

            $scope.ladeDaten = function() {
                MultiHTTP.get(
                    [
                        '/schueler/lehrer',
                        '/faecher',
                        '/klassen'
                    ]
                )
                .then(antwortObjekt => {
                    $scope._data.lehrer = antwortObjekt['/schueler/lehrer'].lehrer;
                    $scope._data.faecher = antwortObjekt['/faecher'];
                    $scope._data.klassen = antwortObjekt['/klassen'];
                })
                .catch(fehler => {
                    alert('Konnte Daten nicht laden');
                })
            }

            $scope.aktualisiereDaten = function() {
                if($scope._auswahl.klasse.uuid && $scope._auswahl.fach.uuid) {
                    $http({
                        method: 'GET',
                        url: '/deputate/' + $scope._auswahl.fach.uuid + '/' + $scope._auswahl.klasse.uuid
                    }).then(function erfolgCallback(antwort) {
                        if(antwort.data.error === false) {
                            $scope._anzeige.lehrer_id = antwort.data.uuid;
                        }
                    }, function fehlerCallback(fehler) {
                        alert('Bücher konnten nicht ausgelesen werden.');
                        alert(JSON.stringify(fehler));
                    });
                }
                
            }

            $scope.speichern = function() {
                $http({
                    method: 'POST',
                    url: '/deputate/' + $scope._auswahl.fach.uuid + '/' + $scope._auswahl.klasse.uuid,
                    data: {
                        schueler_id: $scope._anzeige.lehrer_id
                    }
                })
                .then(antwort => {
                    console.log(antwort);
                    if(antwort.data.error === false) {

                    } else {
                        alert('Fehler beim setzen des Deputats (Server-seitig)');
                    }
                }, fehler => {
                    alert('Fehler beim setzen des Deputats (HTTP)');
                })
            }
            
            $scope.ladeDaten();

		}
})();
