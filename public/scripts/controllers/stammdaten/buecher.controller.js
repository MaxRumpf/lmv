(function() {
    'use strict';
    angular
        .module('app')
        .controller('StammdatenBuecherCtrl', StammdatenBuecherCtrl);
        StammdatenBuecherCtrl.$inject = ['$scope', '$http', 'BuecherService', 'DateService', 'NumService'];
        function StammdatenBuecherCtrl($scope, $http, BuecherService, DateService, NumService) {
            $scope._data = {};
            $scope._data.buecher = [];
            $scope._data.faecher = [];

            $scope._anzeige = {};
            $scope._anzeige.buecher = [];

            $scope._neuesBuch = {
                titel: null,
                verlag: null,
                preis: null,
                stufe_von: null,
                stufe_bis: null,
                isbn: null,
                bestand: null,
                fach_id: null
            };

            $scope._auswahl = {};
            $scope._auswahl.fach = {};
            $scope._auswahl.editiertesBuch = {};

            $scope.setzeAnzeige = function() {
                $scope._anzeige.buecher = [];
                if($scope._auswahl.fach) {
                    for(var i=0;i<$scope._data.buecher.length;i++) {
                        var _buch = $scope._data.buecher[i];
                        if(_buch.fach_id === $scope._auswahl.fach.uuid) {
                            //Buch gehört zu ausgewähltem Fach
                            $scope._anzeige.buecher.push(_buch);
                        }
                    }
                }
            }

            $scope.$watch('_auswahl', _ => {
                $scope.setzeAnzeige();
            }, true);

            $scope.setzeAuswahl = function(typ, objekt) {
                switch(typ) {
                    case 'fach':
                        $scope._auswahl.fach = objekt;
                        break;
                    default:
                        console.log('$scope.setzeAuswahl: unbekannter typ');
                }
            }

            $scope.istAuswahl = function(typ, objekt) {
                switch(typ) {
                    case 'fach':
                        return $scope._auswahl.fach.uuid === objekt.uuid;
                        break;
                    default:
                        console.log('$scope.istAuswahl: unbekannter typ');
                }
            }

            $scope.aktualisiereDaten = function() {
                $http({
                    method: 'GET',
                    url: '/buecher'
                }).then(function erfolgCallback(antwort) {
                    $scope._data.buecher = antwort.data;
                    $http({
                        method: 'GET',
                        url: '/faecher'
                    }).then(antwort => {
                        $scope._data.faecher = antwort.data;
                        if($scope._data.faecher.length > 0 && !$scope._auswahl.fach) {
                            $scope.setzeAuswahl('fach', $scope._data.faecher[0]);
                        }
                        $scope.setzeAnzeige();
                    }, fehler => {
                        alert('Fächer konnten nicht ausgelesen werden.');
                        alert(JSON.stringify(fehler));
                    });
                }, function fehlerCallback(fehler) {
                    alert('Bücher konnten nicht ausgelesen werden.');
                    alert(JSON.stringify(fehler));
                });
            }
            
            $scope.hinzufuegen = function() {
                var neuesBuch = Object.assign({}, $scope._neuesBuch);
                neuesBuch.fach_id = $scope._auswahl.fach.uuid;
                neuesBuch.preis = NumService.DEzuEN($scope._neuesBuch.preis);
                const res = $http.post('/buecher', neuesBuch);
                res.success(function(antwort) {
                    $scope.aktualisiereDaten();
                    $scope._neuesBuch = {};
                });
                res.error(function(fehler) {
                    alert('Buch konnte nicht erstellt werden.');
                    alert(JSON.stringify(fehler));
                })
            }
            $scope.loescheBuch = function(buch) {
                BuecherService.loescheBuch(buch, function erfolgreichCallback() {
                    $scope.aktualisiereDaten();
                }, function fehlerCallback(fehler) {
                    alert('Konnte Buch nicht löschen.');
                    alert(JSON.stringify(fehler));
                })
            }
            $scope.setzeEditieren = function(buch) {
                $scope._auswahl.editiertesBuch = Object.assign({}, buch);
                $scope._auswahl.editiertesBuch.preis = NumService.ENzuDE($scope._auswahl.editiertesBuch.preis);
            }
            $scope.editierenAktiv = function(buch) {
                return buch.uuid === $scope._auswahl.editiertesBuch.uuid;
            }
            $scope.editierenFertig = function() {
                var buch = Object.assign({}, $scope._auswahl.editiertesBuch);
                buch.preis = NumService.DEzuEN(buch.preis);
                BuecherService.aktualisiereBuch(buch, function erfolgreichCallback() {
                    $scope.aktualisiereDaten();
                }, function fehlerCallback(fehler) {
                    alert('Konnte Buch nicht aktualisieren.');
                    alert(JSON.stringify(fehler));
                });
                $scope._auswahl.editiertesBuch = {};
            }
            $scope.editierenAbbrechen = function() {
                $scope._auswahl.editiertesBuch = {};
            }
            $scope.fachString = function(buch) {
                for(var i=0;i<$scope.faecher.length;i++) {
                    var _fach = $scope.faecher[i];
                    if(_fach.uuid === buch.fach_id) {
                        return _fach.bezeichnung;
                    }
                }
            }

            $scope.anzahlBuecherFuerFach = function(fach) {
                var n=0;
                for(var i=0;i<$scope._data.buecher.length;i++) {
                    var _buch = $scope._data.buecher[i];
                    if(_buch.fach_id === fach.uuid) {
                        n++;
                    }
                }
                return n;
            }

            $scope.datumENzuDE = function(en) {
                return DateService.ENzuDE(en);
            }

            $scope.numENzuDE = function(en) {
                return NumService.ENzuDE(en);
            }

            $scope.aktualisiereDaten();

		}
})();
