(function() {
  'use strict';
  angular
  .module('app')
  .controller('StammdatenSchuelerCtrl', StammdatenSchuelerCtrl);
  StammdatenSchuelerCtrl.$inject = ['$scope', '$http', 'SchuelerService', '$stateParams', '$location', 'DateService', '$uibModal'];
  function StammdatenSchuelerCtrl($scope, $http, SchuelerService, $stateParams, $location, DateService, $uibModal) {
    $scope.schueler = [];
    $scope.klassen = [];
    $scope.aktiveKlasse = null;
    $scope.faecher = [];
    $scope.spezialFaecher = {};
    $scope.schuelerInAktuellerKlasse = [];
    $scope.editierterSchueler = {};
    
    $scope.initialisiereAktiveKlasse = function() {
      if(!$scope.aktiveKlasse && $stateParams.klasse && $stateParams.klasse !== "") {
        for(var i=0;i<$scope.klassen.length;i++) {
          if($scope.klassen[i].bezeichnung === $stateParams.klasse) {
            $scope.aktiveKlasse = $scope.klassen[i];
            break;
          }
        }
      } else if(!$scope.aktiveKlasse) {
        $scope.aktiveKlasse = $scope.klassen[0];
      }
    }
    
    $scope.aktualisiereDaten = function() {
      SchuelerService.ladeAlles(function(fehler, antwort) {
        if (fehler) {
          return alert('Konnte Daten nicht laden');
        }
        $scope.schueler = antwort.schueler;
        $scope.klassen = antwort.klassen;
        $scope.faecher = antwort.faecher;
        $scope.spezialFaecher = SchuelerService.extrahiereSpezialfaecher($scope.faecher);
        $scope.fachBezeichnungenAnhandUUID();
        $scope.initialisiereAktiveKlasse();
        $scope.neuerSchueler = SchuelerService.initialisiereNeuerSchuelerObjekt($scope.spezialFaecher);
        $scope.setzeAktuelleSchuelerInKlasse();
      })
    }
    
    $scope.setzeAktiveKlasse = function(klasse) {
      $scope.aktiveKlasse = klasse;
    }
    
    $scope.istAktiveKlasse = function(klasse) {
      return klasse.uuid === $scope.aktiveKlasse.uuid;
    }
    
    $scope.fachBezeichnungenAnhandUUID = function() {
      const typen = ['profil', 'fremdsprache', 'konfession'];
      angular.forEach($scope.schueler, (s, i) => {
        angular.forEach(typen, (typ) => {
          const id = s[typ + '_id'];
          var _i = i;
          SchuelerService.fachBezeichnungAnhandUUID(id, $scope.faecher, (bezeichnung) => {
            $scope.schueler[_i][typ + '_bezeichnung'] = bezeichnung;
          })
        })
        
      });
    }
    
    $scope.loescheSchueler = function (schueler) {
      SchuelerService.loescheSchueler(schueler).then(function erfolgreich() {
        $scope.aktualisiereDaten();
      }, function fehler(f) {
        alert('Schueler konnte nicht gelöscht werden.');
        alert(JSON.stringify(f));
      })
    }
    
    $scope.schuelerHinzufuegen = function() {
      const schueler = {
        name: $scope.neuerSchueler.name,
        geburtsdatum: DateService.DEzuEN($scope.neuerSchueler.geburtsdatum),
        konfession_id: $scope.neuerSchueler.konfession,
        fremdsprache_id: $scope.neuerSchueler.fremdsprache,
        profil_id: $scope.neuerSchueler.profilfach,
        klasse_id: $scope.aktiveKlasse.uuid
      };
      SchuelerService.schuelerHinzufuegen(schueler).then(function erfolgreich() {
        $scope.aktualisiereDaten();
      }, function fehler(f) {
        alert('Schüler konnte nicht hinzugefügt werden.');
        alert(JSON.stringify(f));
      })
    }
    
    $scope.setzeAktuelleSchuelerInKlasse = function() {
      $scope.schuelerInAktuellerKlasse = [];
      angular.forEach($scope.schueler, function(s) {
        if(s.klasse_id === $scope.aktiveKlasse.uuid) {
          $scope.schuelerInAktuellerKlasse.push(s);
        }
      });
    }
    
    $scope.$watch('aktiveKlasse', function(neuerWert) {
      if($scope.aktiveKlasse && $scope.aktiveKlasse.bezeichnung) {
        $location.search('klasse', $scope.aktiveKlasse.bezeichnung);
      }
      $scope.setzeAktuelleSchuelerInKlasse();
    });
    
    $scope.setzeEditieren = function(schueler) {
      $scope.editierterSchueler = Object.assign({}, schueler);
      $scope.editierterSchueler.geburtsdatum = DateService.ENzuDE($scope.editierterSchueler.geburtsdatum);
    }
    
    $scope.editierenAktiv = function(schueler) {
      return schueler.uuid === $scope.editierterSchueler.uuid;
    }
    
    $scope.editierenAbbrechen = function() {
      $scope.editierterSchueler = {};
    }
    
    $scope.editierenFertig = function() {
      const schueler = {
        uuid: $scope.editierterSchueler.uuid,
        name: $scope.editierterSchueler.name,
        geburtsdatum: DateService.DEzuEN($scope.editierterSchueler.geburtsdatum),
        konfession_id: $scope.editierterSchueler.konfession_id,
        fremdsprache_id: $scope.editierterSchueler.fremdsprache_id,
        profil_id: $scope.editierterSchueler.profil_id,
        klasse_id: $scope.editierterSchueler.klasse_id
      };
      if(!schueler.geburtsdatum) {
        return alert('ungültiges Geburtsdatum!');
      }
      $scope.editierterSchueler = {};
      SchuelerService.aktualisiereSchueler(schueler).then(_ => {
        //erfolgreich
        $scope.aktualisiereDaten();
      }, fehler => {
        alert('Daten des Schülers konnten nicht verändert werden.');
        alert(JSON.stringify(fehler));
      })
    }
    
    $scope.anzahlSchuelerFuerKlasse = function(klasse) {
      var n=0;
      for(var i=0;i<$scope.schueler.length;i++) {
        var _schueler = $scope.schueler[i];
        if(_schueler.klasse_id === klasse.uuid) {
          n++;
        }
      }
      return n;
    }
    
    $scope.ENzuDE = function(en) {
      return DateService.ENzuDE(en);
    }
    
    $scope.verschieben = function(schueler) {
      var modalInstance = $uibModal.open({
        templateUrl: '/views/stammdaten/schueler_verschieben.modal.html',
        size: '',
        controller: 'StammdatenSchuelerVerschiebenModalCtrl',
        resolve: {
          schueler: () => {return schueler},
          klassen: () => {return $scope.klassen},
          aktuelleKlasse: () => {return $scope.aktiveKlasse}
        }
      });
      modalInstance.result.then(neueKlasseUUID => {
        //Schüler soll in Klasse mit UUID: neueKlasseUUID verschoben werden
        SchuelerService.verschiebeSchueler(schueler, neueKlasseUUID)
        .then(_ => {
          $scope.aktualisiereDaten();
        }, fehlerNachricht => {
          alert(fehlerNachricht);
        })
      }, _ => {
        //Fenster wurde geschlossen.
      })
    }
    
    $scope.aktualisiereDaten();
  }
})();
