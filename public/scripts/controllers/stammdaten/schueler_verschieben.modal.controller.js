(function() {
    'use strict';
    angular
    .module('app')
    .controller('StammdatenSchuelerVerschiebenModalCtrl', StammdatenSchuelerVerschiebenModalCtrl);
    
    StammdatenSchuelerVerschiebenModalCtrl.$inject = ['$scope', '$uibModalInstance', 'schueler', 'klassen', 'aktuelleKlasse'];
    
    function StammdatenSchuelerVerschiebenModalCtrl($scope, $uibModalInstance, schueler, klassen, aktuelleKlasse) {
        $scope.schueler = schueler;
        $scope.klassen = klassen;
        $scope.aktuelleKlasse = aktuelleKlasse;
        $scope.neueKlasse = aktuelleKlasse.uuid;
        console.log($scope.schueler);
        console.log($scope.klassen);
        $scope.abbrechen = function() {
            $uibModalInstance.dismiss('');
        }

        $scope.verschieben = function() {
            $uibModalInstance.close($scope.neueKlasse);
        }
        
    }
})();
