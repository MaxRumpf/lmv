(function() {
    'use strict';
    angular
        .module('app')
        .controller('StammdatenKlassenCtrl', StammdatenKlassenCtrl);
        StammdatenKlassenCtrl.$inject = ['$scope', '$http', 'KlassenService'];
        function StammdatenKlassenCtrl($scope, $http, KlassenService) {
        	$scope.klassen = [];
            $scope.neueKlasse = {};
            $scope.editierteKlasse = {};
            $scope.aktualisiereDaten = function() {
                $http({
                    method: 'GET',
                    url: '/klassen'
                }).then(function erfolgCallback(antwort) {
                    $scope.klassen = antwort.data;
                }, function fehlerCallback(fehler) {
                    alert('Klassen konnten nicht ausgelesen werden.');
                    alert(JSON.stringify(fehler));
                });
            }
            $scope.aktualisiereDaten();
            $scope.hinzufuegen = function() {
                const res = $http.post('/klassen', {
                    bezeichnung: $scope.neueKlasse.bezeichnung,
                    stufe: $scope.neueKlasse.stufe
                });
                res.success(function(antwort) {
                    $scope.aktualisiereDaten();
                    $scope.neueKlasse.bezeichnung = "";
                    $scope.neueKlasse.stufe = "";
                });
                res.error(function(fehler) {
                    alert('Klasse konnte nicht erstellt werden.');
                    alert(JSON.stringify(fehler));
                })
            }
            $scope.loescheKlasse = function(klasse) {
                KlassenService.loescheKlasse(klasse, function erfolgreichCallback() {
                    console.log(klasse);
                    $scope.aktualisiereDaten();
                }, function fehlerCallback(fehler) {
                    alert('Konnte Klasse nicht löschen.');
                    alert(fehler);
                })
            }
            $scope.setzeEditieren = function(klasse) {
                $scope.editierteKlasse = Object.assign({}, klasse);
            }
            $scope.editierenAktiv = function(klasse) {
                return klasse.uuid === $scope.editierteKlasse.uuid;
            }
            $scope.editierenAbbrechen = function() {
                $scope.editierteKlasse = {};
            }
            $scope.editierenFertig = function() {
                KlassenService.aktualisiereKlasse($scope.editierteKlasse, function erfolgreichCallback() {
                    $scope.aktualisiereDaten();
                }, function fehlerCallback(fehler) {
                    alert('Konnte Klasse nicht aktualisieren.');
                    alert(JSON.stringify(fehler));
                });
                $scope.editierteKlasse = {};
            }
		}
})();
