(function() {
    'use strict';
    angular
        .module('app')
        .controller('StammdatenFaecherCtrl', StammdatenFaecherCtrl);
        StammdatenFaecherCtrl.$inject = ['$scope', '$http', 'FaecherService'];
        function StammdatenFaecherCtrl($scope, $http, FaecherService) {
        	$scope.faecher = [];
            $scope.neuesFach = {typ: 'normal'};
            $scope.editiertesFach = {};
            $scope.aktualisiereDaten = function() {
                $http({
                    method: 'GET',
                    url: '/faecher'
                }).then(function erfolgCallback(antwort) {
                    $scope.faecher = antwort.data;
                }, function fehlerCallback(fehler) {
                    alert('Fächer konnten nicht ausgelesen werden.');
                    alert(JSON.stringify(fehler));
                });
            }
            $scope.aktualisiereDaten();
            $scope.hinzufuegen = function() {
                const res = $http.post('/faecher', {
                    bezeichnung: $scope.neuesFach.bezeichnung,
                    kuerzel: $scope.neuesFach.kuerzel,
                    stufe_von: $scope.neuesFach.stufe_von,
                    stufe_bis: $scope.neuesFach.stufe_bis,
                    typ: $scope.neuesFach.typ
                });
                res.success(function(antwort) {
                    $scope.aktualisiereDaten();
                    $scope.neuesFach.bezeichnung = "";
                    $scope.neuesFach.kuerzel = "";
                    $scope.neuesFach.stufe_von = "";
                    $scope.neuesFach.stufe_bis = "";
                    $scope.neuesFach.typ = "normal";
                });
                res.error(function(fehler) {
                    alert('Fach konnte nicht erstellt werden.');
                    alert(JSON.stringify(fehler));
                })
            }
            $scope.loescheFach = function(fach) {
                FaecherService.loescheFach(fach, function erfolgreichCallback() {
                    $scope.aktualisiereDaten();
                }, function fehlerCallback(fehler) {
                    alert('Konnte Fach nicht löschen.');
                    alert(fehler);
                })
            }
            $scope.setzeEditieren = function(fach) {
                $scope.editiertesFach = Object.assign({}, fach);
            }
            $scope.editierenAktiv = function(fach) {
                return fach.uuid === $scope.editiertesFach.uuid;
            }
            $scope.editierenFertig = function() {
                FaecherService.aktualisiereFach($scope.editiertesFach, function erfolgreichCallback() {
                    $scope.aktualisiereDaten();
                }, function fehlerCallback(fehler) {
                    alert('Konnte Fach nicht aktualisieren.');
                    alert(JSON.stringify(fehler));
                });
                $scope.editiertesFach = {};
            }
            $scope.editierenAbbrechen = function() {
                $scope.editiertesFach = {};
            }
            $scope.typZuString = function(fach) {
                return FaecherService.typZuString(fach);
            }
		}
})();
