(function() {
    'use strict';
    angular
    .module('app')
    .controller('StartseiteCtrl', StartseiteCtrl);
    StartseiteCtrl.$inject = ['$scope', '$http', '$q'];
    function StartseiteCtrl($scope, $http, $q) {
        $scope.buecherAnzahl = 0;
        $scope.schuelerAnzahl = 0;
        $scope.faecherAnzahl = 0;

        $scope.ladeWerte = function() {
            const urls = [
                '/schueler',
                '/faecher',
                '/buecher'
            ];
            const urlCalls = [];
            angular.forEach(urls, function(url) {
                urlCalls.push(
                    $http.get(url)
                );
            });
            $q.all(urlCalls).then(function alleErfolgreich(antworten) {
                var antwortObjekt = {};
                angular.forEach(antworten, function(antwort) {
                    switch(antwort.config.url) {
                        case '/schueler': $scope.schuelerAnzahl = antwort.data.length;
                        case '/faecher': $scope.faecherAnzahl = antwort.data.length;
                        case '/buecher': $scope.buecherAnzahl = antwort.data.length;
                        default:;
                    }
                })
            }, function fehler() {
                alert('Fehler: Statistiken konnten nicht geladen werden');
            });
        }
        $scope.ladeWerte();
    }
})();
