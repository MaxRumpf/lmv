(function() {
    'use strict';
    angular
    .module('app')
    .controller('EinstellungenDatenImportModalCtrl', EinstellungenDatenImportModalCtrl);
    
    EinstellungenDatenImportModalCtrl.$inject = ['$scope', '$uibModalInstance', 'antwort', '$http'];
    
    function EinstellungenDatenImportModalCtrl($scope, $uibModalInstance, antwort, $http) {
        
        $scope.datenAnzahl = antwort.datenAnzahl;
        $scope.parserFehlerAnzahl = antwort.parserFehler.length > 0 ? antwort.parserFehler.length : 0;
        $scope.datenbankFehlerAnzahl = antwort.datenbankFehler.length > 0 ? antwort.datenbankFehler.length : 0;
        $scope.dateiName = antwort.datei;
        $scope.dateiTyp = antwort.dateiTyp;

        $scope.status = 'anfang';

        $scope.importieren = function() {
            $scope.status = 'importieren';
            $http({
                url: '/dateiupload/import/' + $scope.dateiName,
                method: 'POST'
            })
            .then(antwort => {
                console.log('/dateiupload/import antwort: ', antwort.data);
                if(antwort.data.error === false) {
                    $scope.status = 'erfolgreich';
                } else {
                    $scope.status = 'fehler';
                    $scope.fehlerBeschreibung = antwort.data.grund;
                }
            })
            .catch(antwort => {
                console.log('/dateiupload/import antwort http-fehler: ', antwort);
                $scope.status = 'fehler';
            })
        }

        $scope.dismiss = function() {
            $uibModalInstance.dismiss('cancel');
        }
        
    }
})();
