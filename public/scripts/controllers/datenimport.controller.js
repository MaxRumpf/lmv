// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
    'use strict';
    angular
    .module('app')
    .controller('EinstellungenDatenImportCtrl', EinstellungenDatenImportCtrl);
    
    EinstellungenDatenImportCtrl.$inject = ['$scope', 'FileUploader', '$uibModal'];
    
    function EinstellungenDatenImportCtrl($scope, FileUploader, $uibModal) {
        $scope.alleFertig = false;
        
        $scope.datenAnzahl = 0;
        $scope.fehlerAnzahl = 0;
        $scope.datenbankFehler = 0;
        $scope.dateiTyp = '';
        $scope.dateiName = '';
        
        var uploader = $scope.uploader = new FileUploader({
            url: '/dateiupload/upload'
        });
        
        // FILTERS
        
        uploader.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
        
        // CALLBACKS
        
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
            $scope.alleFertig = false;
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, antwort, status, headers) {
            console.info('onCompleteItem', fileItem, antwort, status, headers);
            console.log(antwort);
            if(antwort.error === false) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/views/einstellungen/datenimport.modal.html',
                    controller: 'EinstellungenDatenImportModalCtrl',
                    size: '',
                    resolve: {
                        antwort: antwort
                    }
                });
                $scope.uploader.clearQueue();
            } else {
                alert('Datei konnte nicht hochgeladen/bearbeitet werden.');
            }
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
            $scope.alleFertig = true;
            
        };
        
        console.info('uploader', uploader);
    }
})();
