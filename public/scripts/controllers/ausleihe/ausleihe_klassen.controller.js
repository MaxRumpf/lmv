(function() {
	'use strict';
	angular
	.module('app')
	.controller('AusleiheKlassenCtrl', AusleiheKlassenCtrl);
	
	AusleiheKlassenCtrl.$inject = ['$scope', 'AusleiheKlassenService', '$filter'];
	function AusleiheKlassenCtrl($scope, AusleiheKlassenService, $filter) {
		//Hierarchie: Klasse -> Fach -> Buch -> Liste
		$scope._data = {};
		$scope._data.klassen = [];
		$scope._data.faecher = [];
		$scope._data.buecher = [];
		$scope._data.anzahlen = [];
		
		$scope._anzeige = {};
		$scope._anzeige.faecher = [];
		$scope._anzeige.buecher = [];
		$scope._anzeige.schuelerListe = [];
		
		$scope._auswahl = {};
		$scope._auswahl.klasse = {};
		$scope._auswahl.fach = {};
		$scope._auswahl.buch = {};
		$scope._auswahl.batch = [];
		$scope._auswahl.batchall = [];
		
		$scope.ladeDaten = function() {
			AusleiheKlassenService.ladeAlles().then(daten => {
				//Daten erfolgreich geladen
				$scope._data.klassen = daten.klassen;
				$scope._data.faecher = daten.faecher;
				$scope._data.buecher = daten.buecher;
				$scope._data.anzahlen = daten.anzahlen;
			}, fehler => {
				//Fehler beim laden
				alert('Daten konnten nicht geladen werden.');
				alert(JSON.stringify(fehler));
			});
		}
		
		$scope.setzeAuswahl = function(typ, objekt) {
			switch(typ) {
				case 'klasse':
				$scope._auswahl.klasse = objekt;
				break;
				case 'fach': 
				$scope._auswahl.fach = objekt;
				break;
				case 'buch':
				$scope._auswahl.buch = objekt;
				break;
				default:
				console.log('AusleiheKlassenCtrl.$scope.setzeAuswahl: unbekannter typ');
			}
		}
		
		$scope.$watch('_auswahl', function(neuerWert, alterWert) {
			$scope._anzeige = {};
			$scope._anzeige.faecher = [];
			$scope._anzeige.buecher = [];
			$scope._anzeige.schuelerListe = [];
			
			if(neuerWert.klasse.uuid != alterWert.klasse.uuid || neuerWert.fach.uuid != alterWert.fach.uuid || neuerWert.buch.uuid != alterWert.buch.uuid) {
				$scope._auswahl.batch = [];
			}
			
			//Faecher:
			for(var i=0;i<$scope._data.faecher.length;i++) {
				var _fach = $scope._data.faecher[i];
				if($scope._auswahl.klasse.stufe === 0 || (_fach.stufe_von <= $scope._auswahl.klasse.stufe && _fach.stufe_bis >= $scope._auswahl.klasse.stufe) ) {
					//Fach wird in ausgewählter Klasse unterrichtet
					//wird nun $scope._anzeige.faecher hinzugefügt
					$scope._anzeige.faecher.push(_fach);
				}
			};
			
			//Buecher:
			for(var i=0;i<$scope._data.buecher.length;i++) {
				var _buch = $scope._data.buecher[i];
				if( ($scope._auswahl.klasse.stufe === 0 || (_buch.stufe_von <= $scope._auswahl.klasse.stufe && _buch.stufe_bis >= $scope._auswahl.klasse.stufe)) && _buch.fach_id === $scope._auswahl.fach.uuid) {
					//Buch gehört zu ausgewähltem Fach und wird beim Unterricht in ausgewählter Klasse verwendet
					$scope._anzeige.buecher.push(_buch);
				}
			}
			
			if(!$scope._anzeige.faecher.includes($scope._auswahl.fach)) {
				$scope._auswahl.fach = {};
				$scope._auswahl.batch = [];
			}
			if(!$scope._anzeige.buecher.includes($scope._auswahl.buch)) {
				$scope._auswahl.buch = {};
				$scope._auswahl.batch = [];
			}
			
			if($scope._auswahl.klasse.uuid && $scope._auswahl.fach.uuid && $scope._auswahl.buch.uuid) {
				$scope.ladeBuecherListe();
				$scope.ladeDeputat();
			}
			
			for(var i=0;i<$scope._anzeige.buecher.length;i++) {
				(function(_i) {
					AusleiheKlassenService.buchDaten($scope._auswahl, $scope._anzeige.buecher[_i].uuid)
					.then(antwort => {
						if($scope._anzeige.buecher[_i]) {
							$scope._anzeige.buecher[_i].koennenAusleihen = antwort.koennenAusleihen;
							$scope._anzeige.buecher[_i].habenAusgeliehen = antwort.habenAusgeliehen;
						}
					})
				})(i);
			}
		}, true);
		
		$scope.ladeBuecherListe = function() {
			AusleiheKlassenService.buecherListeFuerAuswahl($scope._auswahl)
			.then(liste => {
				$scope._anzeige.schuelerListe = liste;
				$scope.setzeAlleUUIDS();
			}, fehler => {
				alert('Konnte Buch-Liste nicht vom Server laden');
				console.log(fehler);
			});
		}
		
		$scope.ladeDeputat = function() {
			AusleiheKlassenService.ladeDeputat($scope._auswahl)
			.then(deputat => {
				$scope._anzeige.deputat = deputat;
			})
			.catch(fehler => {
				console.log(fehler);
			})
		}
		
		$scope.setzeAlleUUIDS = function() {
			$scope._auswahl.batchall = [];
			for(var i=0;i<$scope._anzeige.schuelerListe.length;i++) {
				var _data = $scope._anzeige.schuelerListe[i];
				$scope._auswahl.batchall.push(_data.schueler.uuid);
			}
		}
		
		$scope.boolZuString = function(bool) {
			return bool? 'ja':'nein';
		}
		
		$scope.ausleihen = function(objekt) {
			const schueler_id = objekt.schueler.uuid;
			const buch_id = $scope._auswahl.buch.uuid;
			AusleiheKlassenService.add(schueler_id, buch_id)
			.then(_ => {
				$scope.ladeBuecherListe();
			}, fehler => {
				alert('Konnte Status nicht auf ausgeliehen setzen');
				console.log(fehler);
			})
		}
		
		$scope.nichtMehrAusleihen = function(objekt) {
			const schueler_id = objekt.schueler.uuid;
			const buch_id = $scope._auswahl.buch.uuid;
			AusleiheKlassenService.delete(schueler_id, buch_id)
			.then(_ => {
				$scope.ladeBuecherListe();
			}, fehler => {
				alert('Konnte Status nicht auf unausgeliehen setzen');
				console.log(fehler);
			})
		}
		
		$scope.batchAktion = function(aktion) {
			var promise;
			switch(aktion) {
				case 'ausleihen':
				promise = AusleiheKlassenService.auswahlAusleihen($scope._anzeige.schuelerListe, $scope._auswahl.batch, $scope._auswahl.buch.uuid);
				break;
				case 'entleihen':
				promise = AusleiheKlassenService.auswahlEntleihen($scope._anzeige.schuelerListe, $scope._auswahl.batch, $scope._auswahl.buch.uuid);
				break;
				default:
				console.log('AusleiheKlassenCtrl: scope.batchAktion: unbekannte Aktion');
				return;
			}
			promise.then(_ => {
				//erfolgreich
				$scope.ladeBuecherListe();
			})
			.catch(fehler => {
				alert('Aktion "' + aktion + '" konnte nicht auf Auswahl angewendet werden.');
				console.log('AusleiheKlassenCtrl: scope.batchAktion: ', fehler);
			})
		}
		
		$scope.anzahlFuerKlasse = function(id) {
			for(var i=0;i<$scope._data.anzahlen.length;i++) {
				if($scope._data.anzahlen[i].klasse.uuid === id) {
					return $scope._data.anzahlen[i].anzahl;
				}
			}
		}
		
		$scope.drucken = function() {
			var columns = [
				{title: "Schüler-Name", dataKey: "name"},
				{title: "ausgeliehen", dataKey: "ausgeliehen"}
			];
			var insgesamtAnzahl = 0;
			var ausgeliehenAnzahl = 0;
			var rows = $scope._anzeige.schuelerListe.map(objekt => {
				insgesamtAnzahl++;
				if(objekt.ausgeliehen) {
					ausgeliehenAnzahl++
				};
				return {
					name: objekt.schueler.name,
					ausgeliehen: $scope.boolZuString(objekt.ausgeliehen)
				}
			});
			rows = $filter('orderBy')(rows, "name");
			rows.push({name: '', ausgeliehen: ''});
			rows.push({name: 'Insgesamt: ' + insgesamtAnzahl.toString(), ausgeliehen: 'Ausgeliehen: ' + ausgeliehenAnzahl.toString() + '/' + insgesamtAnzahl.toString()});
			const header = 'Klasse: "' + $scope._auswahl.klasse.bezeichnung + '", Fach: "' + $scope._auswahl.fach.kuerzel + '", Buch: "' + $scope._auswahl.buch.titel + '"';
			var doc = new jsPDF('p', 'pt');
			doc.setFontSize(12);
			doc.text(header, 14, 20);
			doc.autoTable(columns, rows, {
				startY: 40,
				theme: 'grid',
				styles: {
					cellPadding: 4
				},
				headerStyles: {
					fillColor: 0,
					textColor: 255
				}
			});
			doc.save($scope._auswahl.klasse.bezeichnung + '-' + $scope._auswahl.fach.kuerzel + '-' + $scope._auswahl.buch.titel + '.pdf');
		}
		
		$scope.ladeDaten();
	}
})();
