(function() {
	'use strict';
	angular
	.module('app')
	.controller('AusleiheBuecherCtrl', AusleiheBuecherCtrl);
	
	AusleiheBuecherCtrl.$inject = ['$scope', 'AusleiheBuecherService', '$state'];
	function AusleiheBuecherCtrl($scope, AusleiheBuecherService, $state) {
		//Hierarchie: Klasse -> Fach -> Buch -> Liste
		$scope._data = {};
		$scope._data.faecher = [];
		$scope._data.buecher = [];
		$scope._data.klassen = [];
		
		$scope._anzeige = {};
		$scope._anzeige.buecher = [];
		$scope._anzeige.schuelerListe = [];
		
		$scope._auswahl = {};
		$scope._auswahl.fach = {};
		$scope._auswahl.buch = {};

		// UUID -> Anzahl
		$scope._ausleiheAnzahlen = {};
		
		$scope.ladeDaten = function() {
			AusleiheBuecherService.ladeAlles().then(daten => {
				//Daten erfolgreich geladen
				$scope._data.faecher = daten.faecher;
				$scope._data.buecher = daten.buecher;
				$scope._data.klassen = daten.klassen;
				$scope.ladeAusleiheAnzahlen();
			}, fehler => {
				//Fehler beim laden
				alert('Daten konnten nicht geladen werden.');
				alert(JSON.stringify(fehler));
			});
		}
		
		$scope.setzeAuswahl = function(typ, objekt) {
			switch(typ) {
				case 'fach': 
				$scope._auswahl.fach = objekt;
				break;
				case 'buch':
				$scope._auswahl.buch = objekt;
				break;
				default:
				console.log('AusleiheBuecherCtrl.$scope.setzeAuswahl: unbekannter typ');
			}
		}
		
		$scope.$watch('_auswahl', function(neuerWert) {
			$scope._anzeige = {};
			$scope._anzeige.buecher = [];
			$scope._anzeige.schuelerListe = [];
			
			//Buecher:
			for(var i=0;i<$scope._data.buecher.length;i++) {
				var _buch = $scope._data.buecher[i];
				if(_buch.fach_id === $scope._auswahl.fach.uuid) {
					//Buch gehört zu ausgewähltem Fach
					$scope._anzeige.buecher.push(_buch);
				}
			}

			if(!$scope._anzeige.buecher.includes($scope._auswahl.buch)) {
				$scope._auswahl.buch = {};
			}
			
			if($scope._auswahl.fach.uuid && $scope._auswahl.buch.uuid) {
				$scope.ladeSchuelerListe();
			}
		}, true);
		
		$scope.ladeAusleiheAnzahlen = function() {
			AusleiheBuecherService.ladeAusleiheAnzahlen()
			.then(anzahlen => {
				$scope._ausleiheAnzahlen = anzahlen;
			}).catch(fehler => {
				alert('Ausleihe-Anzahlen pro Buch konnten nicht geladen werden.');
				console.log(fehler);
			});
		}

		$scope.ladeSchuelerListe = function() {
			AusleiheBuecherService.schuelerListeFuerAuswahl($scope._auswahl)
			.then(liste => {
				$scope._anzeige.schuelerListe = liste;
				$scope.klassenBezeichnungZuSchuelerListe();
				$scope.ladeAusleiheAnzahlen();
			}, fehler => {
				alert('Konnte Schüler-Liste nicht vom Server laden');
				console.log(fehler);
			});
		}
		
		// $scope.boolZuString = function(bool) {
		// 	return bool? 'ja':'nein';
		// }
		
		// $scope.ausleihen = function(objekt) {
		// 	const schueler_id = objekt.schueler.uuid;
		// 	const buch_id = $scope._auswahl.buch.uuid;
		// 	AusleiheBuecherService.add(schueler_id, buch_id)
		// 	.then(_ => {
		// 		$scope.ladeBuecherListe();
		// 	}, fehler => {
		// 		alert('Konnte Status nicht auf ausgeliehen setzen');
		// 		console.log(fehler);
		// 	})
		// }

		$scope.nichtMehrAusleihen = function(objekt) {
			const schueler_id = objekt.schueler.uuid;
			const buch_id = $scope._auswahl.buch.uuid;
			AusleiheBuecherService.delete(schueler_id, buch_id)
			.then(_ => {
				$scope.ladeSchuelerListe();
			}, fehler => {
				alert('Konnte Status nicht auf unausgeliehen setzen');
				console.log(fehler);
			})
		}

		$scope.ausleiheAnzahl = function(buch) {
			return $scope._ausleiheAnzahlen[buch.uuid];
		}

		$scope.klasseFuerUUID = function(uuid) {
			var klassenMitUUID = $scope._data.klassen.filter(klasse => klasse.uuid === uuid);
			return klassenMitUUID[0]? klassenMitUUID[0] : null;
		} 

		$scope.klassenBezeichnungZuSchuelerListe = function() {
			$scope._anzeige.schuelerListe = $scope._anzeige.schuelerListe.map(objekt => {
				objekt.schueler.klasse = $scope.klasseFuerUUID(objekt.schueler.klasse_id);
				return objekt;
			})
		}
		
		$scope.ladeDaten();
	}
})();
