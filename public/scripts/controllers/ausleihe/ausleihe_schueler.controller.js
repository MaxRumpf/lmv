(function() {
	'use strict';
	angular
	.module('app')
	.controller('AusleiheSchuelerCtrl', AusleiheSchuelerCtrl);
	
	AusleiheSchuelerCtrl.$inject = ['$scope', 'AusleiheSchuelerService', '$stateParams', '$location', '$filter'];
	function AusleiheSchuelerCtrl($scope, AusleiheSchuelerService, $stateParams, $location, $filter) {
		//Hierarchie: Klasse -> Fach -> Buch -> Liste
		$scope._data = {};
		$scope._data.klassen = [];
		$scope._data.faecher = [];
		
		$scope._anzeige = {};
		$scope._anzeige.schueler = [];
		$scope._anzeige.buecherListe = [];
		
		$scope._auswahl = {};
		$scope._auswahl.klasse = {};
		$scope._auswahl.schueler = {};
		$scope._auswahl.batch = [];
		$scope._auswahl.batchall = [];
		
		$scope.schuelerZuSetzen = null;

		$scope.ladeDaten = function() {
			AusleiheSchuelerService.ladeAlles().then(daten => {
				//Daten erfolgreich geladen
				$scope._data.klassen = daten.klassen;
				$scope._data.klassen.push({uuid: '*', bezeichnung: '*', stufe: 0});
				$scope._data.faecher = daten.faecher;
				if($stateParams.klasse) {
					for(var a=0;a<$scope._data.klassen.length;a++) {
						var _klasse = $scope._data.klassen[a];
						if(_klasse.uuid === $stateParams.klasse) {
							$scope._auswahl.klasse = _klasse;
							for(var b=0;b<$scope._data.faecher.length;b++) {
								var _fach = $scope._data.faecher[b];
								if(_fach.kuerzel === 'DE') {
									$scope._auswahl.fach = _fach;
									$scope.schuelerZuSetzen = $stateParams.schueler;
								}
							}
						}
					}
				}
			}, fehler => {
				//Fehler beim laden
				alert('Daten konnten nicht geladen werden.');
				alert(JSON.stringify(fehler));
			});
		}
		
		$scope.setzeAuswahl = function(typ, objekt) {
			switch(typ) {
				case 'klasse':
				$scope._auswahl.klasse = objekt;
				break;
				case 'schueler':
				$scope._auswahl.schueler = objekt;
				break;
				default:
				console.log('AusleiheSchuelerCtrl.$scope.setze_Auswahl: unbekannter typ');
			}
		}
		
		$scope.$watch('_auswahl', function(neuerWert, alterWert) {
			$scope._anzeige = {};
			$scope._anzeige.schueler = [];
			$scope._anzeige.buecherListe = [];
			
			if(neuerWert.klasse.uuid != alterWert.klasse.uuid || neuerWert.schueler.uuid != alterWert.schueler.uuid) {
				$scope._auswahl.batch = [];
			}
			
			//Schueler:
			if($scope._auswahl.klasse.uuid) {
				$scope.ladeSchueler();
			}

			if($scope._auswahl.klasse.bezeichnung !== '*' && $scope._auswahl.schueler.klasse_id != $scope._auswahl.klasse.uuid) {
				$scope._auswahl.schueler = {};
				$scope._auswahl.batch = [];
				$scope._anzeige.buecherListe = [];
			}

		}, true);
		
		$scope.ladeBuecherListe = function() {
			AusleiheSchuelerService.buecherListeFuerAuswahl($scope._auswahl)
			.then(liste => {
				$scope._anzeige.buecherListe = liste;
				$scope.fachBezeichnungenZuBuecherListe();
				$scope.setzeAlleUUIDS();
			}, fehler => {
				alert('Konnte Büecher-Liste nicht vom Server laden');
				console.log(fehler);
			});
		}

		$scope.setzeAlleUUIDS = function() {
			$scope._auswahl.batchall = [];
			for(var i=0;i<$scope._anzeige.buecherListe.length;i++) {
				var _data = $scope._anzeige.buecherListe[i];
				$scope._auswahl.batchall.push(_data.buch.uuid);
			}
		}

		$scope.ladeSchueler = function() {
			AusleiheSchuelerService.schuelerInKlasse($scope._auswahl.klasse)
			.then(schueler => {
				$scope._anzeige.schueler = schueler;
				if($scope._anzeige.schueler.length === 0) {
					$scope._auswahl.batch = [];
					$scope._anzeige.buecherListe = [];
					$scope._auswahl.schueler = {};
				} else {
					var ausgewaehlterSchulerInSchuelerListe = false;
					for(var i=0;i<$scope._anzeige.schueler.length;i++) {
						if($scope._anzeige.schueler[i].uuid === $scope._auswahl.schueler.uuid) {
							ausgewaehlterSchulerInSchuelerListe = true;
						}
					}
					if(ausgewaehlterSchulerInSchuelerListe && $scope._auswahl.klasse.uuid && $scope._auswahl.schueler.uuid) {
						$scope.ladeBuecherListe();
					} else {
						$scope._anzeige.buecherListe = [];
						$scope._auswahl.batch = [];
						$scope._auswahl.schueler = {};
						if($scope.schuelerZuSetzen) {
							for(var i=0;i<$scope._anzeige.schueler.length;i++) {
								var _schueler = $scope._anzeige.schueler[i];
								if(_schueler.uuid === $scope.schuelerZuSetzen) {
									$scope.schuelerZuSetzen = null;
									$scope._auswahl.schueler = _schueler;
									$location.search('klasse', '');
									$location.search('schueler', '');
								}
							}
						}
					}
				}
			}, fehler => {
				alert('Konnte Schüler nicht laden.');
				console.log(fehler);
			})
		}
		
		$scope.boolZuString = function(bool) {
			return bool? 'ja':'nein';
		}
		
		$scope.ausleihen = function(objekt) {
			const schueler_id = $scope._auswahl.schueler.uuid;
			const buch_id = objekt.buch.uuid;
			AusleiheSchuelerService.add(schueler_id, buch_id)
			.then(_ => {
				$scope.ladeBuecherListe();
			}, fehler => {
				alert('Konnte Status nicht auf ausgeliehen setzen');
				console.log(fehler);
			})
		}

		$scope.nichtMehrAusleihen = function(objekt) {
			const schueler_id = $scope._auswahl.schueler.uuid;
			const buch_id = objekt.buch.uuid;
			AusleiheSchuelerService.delete(schueler_id, buch_id)
			.then(_ => {
				$scope.ladeBuecherListe();
			}, fehler => {
				alert('Konnte Status nicht auf unausgeliehen setzen');
				console.log(fehler);
			})
		}
		
		$scope.batchAktion = function(aktion) {
			var promise;
			switch(aktion) {
				case 'ausleihen':
					promise = AusleiheSchuelerService.auswahlAusleihen($scope._anzeige.buecherListe, $scope._auswahl.batch, $scope._auswahl.schueler.uuid);
					break;
				case 'entleihen':
					promise = AusleiheSchuelerService.auswahlEntleihen($scope._anzeige.buecherListe, $scope._auswahl.batch, $scope._auswahl.schueler.uuid);
					break;
				default:
					console.log('AusleiheSchuelerCtrl: scope.batchAktion: unbekannte Aktion');
					return;
			}
			promise.then(_ => {
				//erfolgreich
				$scope.ladeBuecherListe();
			})
			.catch(fehler => {
				alert('Aktion "' + aktion + '" konnte nicht auf Auswahl angewendet werden.');
				console.log('AusleiheSchuelerCtrl: scope.batchAktion: ', fehler);
			})
		}

		$scope.fachFuerUUID = function(uuid) {
			var fachMitUUID = $scope._data.faecher.filter(fach => fach.uuid === uuid);
			return fachMitUUID[0]? fachMitUUID[0].bezeichnung : 'unbekanntes Fach';
		}

		$scope.fachBezeichnungenZuBuecherListe = function() {
			$scope._anzeige.buecherListe = $scope._anzeige.buecherListe.map(objekt => {
				objekt.buch.fach_bezeichnung = $scope.fachFuerUUID(objekt.buch.fach_id);
				return objekt;
			})
		}

		$scope.drucken = function() {
			var columns = [
				{title: "Fach", dataKey: "fach"},
				{title: "Buch-Titel", dataKey: "titel"},
				{title: "ausgeliehen", dataKey: "ausgeliehen"},
			];
			var rows = $scope._anzeige.buecherListe.map(objekt => {
				return {
					fach: objekt.buch.fach_bezeichnung,
					titel: objekt.buch.titel,
					ausgeliehen: $scope.boolZuString(objekt.ausgeliehen)
				}
			});
			rows = $filter('orderBy')(rows, "['fach', 'titel']");
			const header = 'Klasse: "' + $scope._auswahl.klasse.bezeichnung + '", Schüler: "' + $scope._auswahl.schueler.name + '"';
			var doc = new jsPDF('p', 'pt');
			doc.setFontSize(12);
			doc.text(header, 14, 20);
			doc.autoTable(columns, rows, {
				startY: 40,
				theme: 'grid',
				styles: {
					cellPadding: 4
				},
				headerStyles: {
					fillColor: 0,
					textColor: 255
				}
			});
			doc.save($scope._auswahl.klasse.bezeichnung + '-' + $scope._auswahl.schueler.name + '.pdf');
		}
		
		$scope.ladeDaten();
	}
})();
