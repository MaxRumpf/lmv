(function() {
  'use strict';
  angular
  .module('app')
  .directive('batchEinzeln', function() {
    return {
      scope: {
        uuid: '=',
        data: '='
      },
      template: '<label class="md-check"><input type="checkbox"> <i class="{{app.setting.theme.primary}}"></i></label>',
      link: function($scope, elem, attrs, ctrls) {
        var checkbox = $(elem).find('input');
        setState();
        function setState() {
          checkbox.prop('checked', $scope.data.includes($scope.uuid));
        }
        checkbox.bind('change', function() {
          var newState = checkbox.is(':checked');
          if(newState === true) {
            //War nicht ausgewählt, ist jetzt ausgewählt
            $scope.data.push($scope.uuid);
          } else {
            //War ausgewählt, ist jetzt nichtmehr ausgewählt
            $scope.data = $scope.data.filter(e => e !== $scope.uuid)
          }
          $scope.$apply();
        })
      }
    }
  })
  .directive('batchAllesCheckbox', function() {
    return {
      scope: {
        all: '=',
        data: '='
      },
      template: '<label class="md-check"><input type="checkbox"> <i class="{{app.setting.theme.primary}}"></i></label>',
      link: function($scope, elem, attrs, ctrls) {
        upd();
        $scope.$watch('data', _ => {
          upd();
        }, true);
        $scope.$watch('all', _ => {
          upd();
        }, true);
        function upd() {
          if(!checkbox) {return}
          if($scope.data.length != $scope.all.length) {
            checkbox.prop('checked', false);
          } else {
            checkbox.prop('checked', true);
          }
        }
        var checkbox = $(elem).find('input');
        checkbox.prop('checked', false);
        checkbox.bind('change', function() {
          var newState = checkbox.is(':checked');
          if(newState === true) {
            //Alles auswählen
            $scope.data = $scope.all;
          } else {
            //War ausgewählt, ist jetzt nichtmehr ausgewählt
            $scope.data = [];
          }
          $scope.$apply();
        })
      }
    }
  })
  .directive('batchAllesButton', function() {
    return {
      scope: {
        all: '=',
        data: '=',
        colour: '@'
      },
      template: '<button ng-if="show" class="btn btn-sm {{colour}}" ng-click="toggle()">{{text}}</button>',
      controller: ['$scope', function($scope) {
        $scope.show = false;
        $scope.nichtAlles = true;
        $scope.text = "alles";
        upd();
        $scope.$watch('data', _ => {
          upd();
        }, true);
        $scope.$watch('all', _ => {
          upd();
        }, true);
        function upd() {
          if($scope.all.length === 0) {
            $scope.text = "alles";
            $scope.nichtAlles = true;
            $scope.show = false;
            return;
          }
          $scope.show = true;
          if($scope.data.length !== $scope.all.length) {
            $scope.nichtAlles = true;
            $scope.text = "alles";
          } else {
            $scope.nichtAlles = false;
            $scope.text = "nichts";
          }
        }
        $scope.toggle = function() {
          if($scope.nichtAlles) {
            $scope.data = $scope.all;
          } else {
            $scope.data = [];
          } 
        }
      }]
    }
  });
})();