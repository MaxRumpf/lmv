(function() {
    'use strict';
    angular
        .module('app')
        .service('MultiHTTP', MultiHTTP);
        MultiHTTP.$inject = ['$http', '$q']
        function MultiHTTP($http, $q) {
            return {
                get: function(urls, cb) {
                    //cb(error, antwort)
                    var deferred = $q.defer();
                    const urlCalls = [];
                    angular.forEach(urls, function(url) {
                        urlCalls.push(
                            $http.get(url)
                        );
                    });
                    $q.all(urlCalls).then(function alleErfolgreich(antworten) {
                        var antwortObjekt = {};
                        var fehler = false;
                        for(var i=0;i<antworten.length;i++) {
                            var _antwort = antworten[i];
                            if(_antwort.data.error === true) {
                                fehler = true;
                                break;
                            }
                            antwortObjekt[_antwort.config.url] = _antwort.data;
                        }
                        if(fehler) {
                            deferred.reject('Datenbank-Fehler');
                        } else {
                            deferred.resolve(antwortObjekt);
                        }
                    }, function fehler(f) {
                        deferred.reject(f);
                    });
                    return deferred.promise;
                }
            };
        }
})();
