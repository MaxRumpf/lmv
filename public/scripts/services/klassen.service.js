(function() {
    'use strict';
    angular
        .module('app')
        .service('KlassenService', KlassenService);
        KlassenService.$inject = ['$http', '$q']
        function KlassenService($http, $q) {
            return {
                aktualisiereKlasse: function(klasse, erfolgreichCallback, fehlerCallback) {
                	$http({
                		method: 'PUT',
                		url: '/klassen/' + klasse.uuid,
                		data: klasse
                	}).then(erfolgreichCallback, fehlerCallback);
                },
                loescheKlasse: function(klasse, erfolgreichCallback, fehlerCallback) {
                	console.log(klasse);
                	$http({
                		method: 'DELETE',
                		url: '/klassen/' + klasse.uuid
                	}).then(antwort => {
                        if(antwort.data.error === false) {
                            erfolgreichCallback();
                        } else {
                            fehlerCallback(antwort.data.grund);
                        }
                    }).catch(fehler => {
                        fehlerCallback(fehler);
                    });
                }
            };
        }
})();
