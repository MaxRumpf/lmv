(function() {
    'use strict';
    angular
        .module('app')
        .service('SchuelerService', SchuelerService);
        SchuelerService.$inject = ['$http', '$q']
        function SchuelerService($http, $q) {
            return {
                ladeAlles: function(cb) {
                    //cb(error, antwort)
                    const urls = [
                        '/schueler',
                        '/faecher',
                        '/klassen'
                    ];
                    const urlCalls = [];
                    angular.forEach(urls, function(url) {
                        urlCalls.push(
                            $http.get(url)
                        );
                    });
                    $q.all(urlCalls).then(function alleErfolgreich(antworten) {
                        var antwortObjekt = {};
                        angular.forEach(antworten, function(antwort) {
                            switch(antwort.config.url) {
                                case '/schueler': antwortObjekt.schueler = antwort.data;
                                case '/faecher': antwortObjekt.faecher = antwort.data;
                                case '/klassen': antwortObjekt.klassen = antwort.data;
                                default:;
                            }
                        })
                        cb(null, antwortObjekt);
                    }, function fehler() {
                        return cb(true, null);
                    });
                },
                extrahiereSpezialfaecher: function(faecher) {
                    //Arten von Fächern: Normal, Profilfach, Fremdsprache, Konfession
                    var antwort = {
                        profilfaecher: [],
                        fremdsprachen: [],
                        konfessionen: []
                    };
                    angular.forEach(faecher, function(fach) {
                        switch(fach.typ) {
                            case 'profil': 
                                antwort.profilfaecher.push(fach);
                                break;
                            case 'fremdsprache':
                                antwort.fremdsprachen.push(fach);
                                break;
                            case 'konfession':
                                antwort.konfessionen.push(fach);
                                break;
                            default: break;
                        }
                    });
                    return antwort;
                },
                fachBezeichnungAnhandUUID: function(uuid, faecher, cb) {
                    var n = 0;
                    angular.forEach(faecher, (fach, i, arr) => {
                        if (Object.is(arr.length - 1, i) && fach.uuid !== uuid && n==0) {
                            return cb('nichts');
                        } else if (fach.uuid === uuid) {
                            n = 1;
                            return cb(fach.bezeichnung);
                        }
                    });
                },
                initialisiereNeuerSchuelerObjekt: function(spezialFaecher) {
                    var neuerSchueler = {};
                    if(spezialFaecher.hasOwnProperty('konfessionen') && spezialFaecher['konfessionen'].length != 0) {
                        neuerSchueler.konfession = spezialFaecher['konfessionen'][0].uuid;
                    }
                    if(spezialFaecher.hasOwnProperty('fremdsprachen') && spezialFaecher['fremdsprachen'].length != 0) {
                        neuerSchueler.fremdsprache = spezialFaecher['fremdsprachen'][0].uuid;
                    }
                    if(spezialFaecher.hasOwnProperty('profilfaecher') && spezialFaecher['profilfaecher'].length != 0) {
                        neuerSchueler.profilfach = spezialFaecher['profilfaecher'][0].uuid;
                    }
                    return neuerSchueler;
                },
                loescheSchueler: function(schueler) {
                    var deferred = $q.defer();
                    if(!schueler || !schueler.uuid) { //prüft für '', null, undefined, false, 0, NaN
                        deferred.reject('Schueler Objekt nicht vorhanden oder keine UUID');
                    } else {
                        $http.delete('/schueler/' + schueler.uuid).then(function erfolgreich() {
                            deferred.resolve();
                        }, function fehler(f) {
                            deferred.reject(f);
                        })
                    }
                    return deferred.promise;
                },
                schuelerHinzufuegen: function(schueler) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: '/schueler',
                        data: schueler
                    }).then(response => {
                        if(response.data.error === 'true') {
                            deferred.reject('Interner Server-Fehler');
                        } else {
                            deferred.resolve();
                        }
                    }, f => {
                        deferred.reject(f);
                    });
                    return deferred.promise;
                },
                aktualisiereSchueler: function(schueler) {
                    var deferred = $q.defer();
                    $http({
                        method: 'PUT',
                        url: '/schueler/' + schueler.uuid,
                        data: schueler
                    }).then(antwort => {
                        //kein HTTP Fehlercode
                        if(antwort.data.error === false) {
                            deferred.resolve();
                        } else {
                            deferred.reject('Server-Fehler');
                        }
                    }, fehler => {
                        //HTTP Fehlercode
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                },
                verschiebeSchueler: function(schueler, neueKlasseUUID) {
                    var deferred = $q.defer();
                    $http({
                        url: '/schueler/verschieben/' + schueler.uuid,
                        method: 'POST',
                        data: {neueKlasse: neueKlasseUUID}
                    })
                    .then(antwort => {
                        if(antwort.data.error === false) {
                            deferred.resolve();
                        } else {
                            deferred.reject('Schüler konnte nicht verschoben werden.');
                        }
                    }, fehler => {
                        deferred.reject('Schüler konnte aufgrund von HTTP-Fehler nicht verschoben werden.');
                    });
                    return deferred.promise;
                }
            };
        }
})();
