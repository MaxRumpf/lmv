(function() {
    'use strict';
    angular
        .module('app')
        .service('AusleiheSchuelerService', AusleiheSchuelerService);
        AusleiheSchuelerService.$inject = ['$http', '$q']
        function AusleiheSchuelerService($http, $q) {
            return {
                ladeAlles: function(cb) {
                    //cb(error, antwort)
                    var deferred = $q.defer();
                    const urls = [
                        '/klassen',
                        '/faecher'
                    ];
                    const urlCalls = [];
                    angular.forEach(urls, function(url) {
                        urlCalls.push(
                            $http.get(url)
                        );
                    });
                    $q.all(urlCalls).then(function alleErfolgreich(antworten) {
                        var antwortObjekt = {};
                        var fehler = false;
                        for(var i=0;i<antworten.length;i++) {
                            var _antwort = antworten[i];
                            if(_antwort.data.error === true) {
                                fehler = true;
                                break;
                            }
                            switch(_antwort.config.url) {
                                case '/klassen': 
                                    antwortObjekt.klassen = _antwort.data;
                                    break;
                                case '/faecher': 
                                    antwortObjekt.faecher = _antwort.data;
                                    break;
                                default:;
                            }
                        }
                        if(fehler) {
                            deferred.reject('Datenbank-Fehler');
                        } else {
                            deferred.resolve(antwortObjekt);
                        }
                    }, function fehler(f) {
                        deferred.reject(f);
                    });
                    return deferred.promise;
                }, 
                buecherListeFuerAuswahl: function(auswahl) {
                    var deferred = $q.defer();
                    const schueler_id = auswahl.schueler.uuid;
                    $http.get('/ausleihe/liste/s/' + schueler_id)
                    .then(data => {
                        deferred.resolve(data.data);
                    }, fehler => {
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                },
                add: function(schueler_id, buch_id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: '/ausleihe/',
                        data: {
                            schueler_id: schueler_id,
                            buch_id: buch_id
                        }
                    })
                    .then(antwort => {
                        if(antwort.error === true) {
                            deferred.reject('Server-Fehler');
                        } else {
                            deferred.resolve();
                        }
                    }, fehler => {
                        deferred.reject(fehler);
                    });
                    return deferred.promise;
                },
                delete: function(schueler_id, buch_id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'DELETE',
                        url: '/ausleihe/' + schueler_id + '/' + buch_id
                    })
                    .then(antwort => {
                        if(antwort.error === true) {
                            deferred.reject('Server-Fehler');
                        } else {
                            deferred.resolve();
                        }
                    }, fehler => {
                        deferred.reject(fehler);
                    });
                    return deferred.promise;
                },
                schuelerInKlasse: function(klasse, fach) {
                    var deferred = $q.defer();

                    $http.get('/schueler/' + klasse.uuid)
                    .then(antwort => {
                        if(antwort.data.error === false) {
                            deferred.resolve(antwort.data.data);
                        } else {
                            deferred.reject('Server-Fehler');
                        }
                    }, fehler => {
                        deferred.reject('HTTP Fehler');
                    });

                    return deferred.promise;
                },
                auswahlAusleihen: function(buecherListe, auswahlUUIDs, schuelerID) {
                    var deferred = $q.defer();
                    var queries = [];
                    for(var i=0;i<buecherListe.length;i++) {
                        //buecherListe: Datenformat [{schueler: Schüler-Objekt aus Datenbank, ausgeliehen: true/false}]
                        var buchObjekt = buecherListe[i];
                        if(auswahlUUIDs.includes(buchObjekt.buch.uuid)) {
                            if(!buchObjekt.ausgeliehen) {
                                queries.push(this.add(schuelerID, buchObjekt.buch.uuid));
                            }
                        }
                    }
                    $q.all(queries)
                    .then(_ => {
                        deferred.resolve();
                    })
                    .catch(fehler => {
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                },
                auswahlEntleihen: function(buecherListe, auswahlUUIDs, schuelerID) {
                    var deferred = $q.defer();
                    var queries = [];
                    for(var i=0;i<buecherListe.length;i++) {
                        //buecherListe: Datenformat [{schueler: Schüler-Objekt aus Datenbank, ausgeliehen: true/false}]
                        var buchObjekt = buecherListe[i];
                        if(auswahlUUIDs.includes(buchObjekt.buch.uuid)) {
                            if(buchObjekt.ausgeliehen) {
                                queries.push(this.delete(schuelerID, buchObjekt.buch.uuid));
                            }
                        }
                    }
                    $q.all(queries)
                    .then(_ => {
                        deferred.resolve();
                    })
                    .catch(fehler => {
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                }
            };
        }
})();
