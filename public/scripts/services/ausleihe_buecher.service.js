(function() {
    'use strict';
    angular
        .module('app')
        .service('AusleiheBuecherService', AusleiheBuecherService);
        AusleiheBuecherService.$inject = ['$http', '$q']
        function AusleiheBuecherService($http, $q) {
            return {
                ladeAlles: function(cb) {
                    //cb(error, antwort)
                    var deferred = $q.defer();
                    const urls = [
                        '/faecher',
                        '/buecher',
                        '/klassen'
                    ];
                    const urlCalls = [];
                    angular.forEach(urls, function(url) {
                        urlCalls.push(
                            $http.get(url)
                        );
                    });
                    $q.all(urlCalls).then(function alleErfolgreich(antworten) {
                        var antwortObjekt = {};
                        var fehler = false;
                        for(var i=0;i<antworten.length;i++) {
                            var _antwort = antworten[i];
                            if(_antwort.data.error === true) {
                                fehler = true;
                                break;
                            }
                            switch(_antwort.config.url) {
                                case '/faecher': 
                                    antwortObjekt.faecher = _antwort.data;
                                    break;
                                case '/buecher': 
                                    antwortObjekt.buecher = _antwort.data;
                                    break;
                                case '/klassen':
                                    antwortObjekt.klassen = _antwort.data;
                                default:;
                            }
                        }
                        if(fehler) {
                            deferred.reject('Datenbank-Fehler');
                        } else {
                            deferred.resolve(antwortObjekt);
                        }
                    }, function fehler(f) {
                        deferred.reject(f);
                    });
                    return deferred.promise;
                }, 
                schuelerListeFuerAuswahl: function(auswahl) {
                    var deferred = $q.defer();
                    const buch_id = auswahl.buch.uuid;
                    $http.get('/ausleihe/liste/b/'+ buch_id)
                    .then(antwort => {
                        if(antwort.data.error === false) {
                            deferred.resolve(antwort.data.data);
                        }
                        deferred.reject('Datenbank-Fehler');
                    }, fehler => {
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                },
                // add: function(schueler_id, buch_id) {
                //     var deferred = $q.defer();
                //     $http({
                //         method: 'POST',
                //         url: '/ausleihe/',
                //         data: {
                //             schueler_id: schueler_id,
                //             buch_id: buch_id
                //         }
                //     })
                //     .then(antwort => {
                //         if(antwort.error === true) {
                //             deferred.reject('Server-Fehler');
                //         } else {
                //             deferred.resolve();
                //         }
                //     }, fehler => {
                //         deferred.reject(fehler);
                //     });
                //     return deferred.promise;
                // },
                delete: function(schueler_id, buch_id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'DELETE',
                        url: '/ausleihe/' + schueler_id + '/' + buch_id
                    })
                    .then(antwort => {
                        if(antwort.error === true) {
                            deferred.reject('Server-Fehler');
                        } else {
                            deferred.resolve();
                        }
                    }, fehler => {
                        deferred.reject(fehler);
                    });
                    return deferred.promise;
                },
                ladeAusleiheAnzahlen: function() {
                    var deferred = $q.defer();
                    $http.get('/ausleihe/anzahlen')
                    .then(antwort => {
                        if(antwort.data.error === false) {
                            deferred.resolve(antwort.data.data);
                        } else {
                            deferred.reject('Server-Fehler');
                        }
                    })
                    .catch(fehler => {
                        deferred.reject(fehler);
                    });
                    return deferred.promise;
                }
            };
        }
})();
