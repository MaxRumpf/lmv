(function() {
    'use strict';
    angular
        .module('app')
        .service('FaecherService', FaecherService);
        FaecherService.$inject = ['$http', '$q']
        function FaecherService($http, $q) {
            return {
                typZuString: function(fach) {
                    switch(fach.typ) {
                    	case 'normal': return "Normal";
                    	case 'fremdsprache': return 'Fremdsprache';
                    	case 'profil': return 'Profil';
                    	case 'konfession': return 'Konfession';
                    	default: return 'Fehler';
                    }
                },
                aktualisiereFach: function(fach, erfolgreichCallback, fehlerCallback) {
                	$http({
                		method: 'PUT',
                		url: '/faecher/' + fach.uuid,
                		data: fach
                	}).then(erfolgreichCallback, fehlerCallback);
                },
                loescheFach: function(fach, erfolgreichCallback, fehlerCallback) {
                	console.log(fach);
                	$http({
                		method: 'DELETE',
                		url: '/faecher/' + fach.uuid
                	}).then(antwort => {
                        if(antwort.data.error === false) {
                            erfolgreichCallback();
                        } else {
                            fehlerCallback(antwort.data.grund);
                        }
                    }).catch(fehler => {
                        fehlerCallback(fehler);
                    });
                }
            };
        }
})();
