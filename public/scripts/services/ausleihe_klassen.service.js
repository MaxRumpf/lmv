(function() {
    'use strict';
    angular
        .module('app')
        .service('AusleiheKlassenService', AusleiheKlassenService);
        AusleiheKlassenService.$inject = ['$http', '$q']
        function AusleiheKlassenService($http, $q) {
            return {
                ladeAlles: function(cb) {
                    //cb(error, antwort)
                    var deferred = $q.defer();
                    const urls = [
                        '/klassen',
                        '/faecher',
                        '/buecher',
                        '/klassen/anzahlen'
                    ];
                    const urlCalls = [];
                    angular.forEach(urls, function(url) {
                        urlCalls.push(
                            $http.get(url)
                        );
                    });
                    $q.all(urlCalls).then(function alleErfolgreich(antworten) {
                        var antwortObjekt = {};
                        var fehler = false;
                        for(var i=0;i<antworten.length;i++) {
                            var _antwort = antworten[i];
                            if(_antwort.data.error === true) {
                                fehler = true;
                                break;
                            }
                            switch(_antwort.config.url) {
                                case '/klassen': 
                                    antwortObjekt.klassen = _antwort.data;
                                    break;
                                case '/faecher': 
                                    antwortObjekt.faecher = _antwort.data;
                                    break;
                                case '/buecher': 
                                    antwortObjekt.buecher = _antwort.data;
                                    break;
                                case '/klassen/anzahlen':
                                    antwortObjekt.anzahlen = _antwort.data.data;
                                    break;
                                default:;
                            }
                        }
                        if(fehler) {
                            deferred.reject('Datenbank-Fehler');
                        } else {
                            deferred.resolve(antwortObjekt);
                        }
                    }, function fehler(f) {
                        deferred.reject(f);
                    });
                    return deferred.promise;
                }, 
                buecherListeFuerAuswahl: function(auswahl) {
                    var deferred = $q.defer();
                    const klasse_id = auswahl.klasse.uuid;
                    const fach_id = auswahl.fach.uuid;
                    const buch_id = auswahl.buch.uuid;
                    $http.get('/ausleihe/liste/kfb/' + klasse_id + '/' + fach_id + '/' + buch_id)
                    .then(data => {
                        deferred.resolve(data.data);
                    }, fehler => {
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                },
                add: function(schueler_id, buch_id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: '/ausleihe/',
                        data: {
                            schueler_id: schueler_id,
                            buch_id: buch_id
                        }
                    })
                    .then(antwort => {
                        if(antwort.error === true) {
                            deferred.reject('Server-Fehler');
                        } else {
                            deferred.resolve();
                        }
                    }, fehler => {
                        deferred.reject(fehler);
                    });
                    return deferred.promise;
                },
                delete: function(schueler_id, buch_id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'DELETE',
                        url: '/ausleihe/' + schueler_id + '/' + buch_id
                    })
                    .then(antwort => {
                        if(antwort.error === true) {
                            deferred.reject('Server-Fehler');
                        } else {
                            deferred.resolve();
                        }
                    }, fehler => {
                        deferred.reject(fehler);
                    });
                    return deferred.promise;
                },
                auswahlAusleihen: function(schuelerListe, auswahlUUIDs, buchID) {
                    var deferred = $q.defer();
                    var queries = [];
                    for(var i=0;i<schuelerListe.length;i++) {
                        //schuelerListe: Datenformat [{schueler: Schüler-Objekt aus Datenbank, ausgeliehen: true/false}]
                        var schuelerObjekt = schuelerListe[i];
                        if(auswahlUUIDs.includes(schuelerObjekt.schueler.uuid)) {
                            if(!schuelerObjekt.ausgeliehen) {
                                queries.push(this.add(schuelerObjekt.schueler.uuid, buchID));
                            }
                        }
                    }
                    $q.all(queries)
                    .then(_ => {
                        deferred.resolve();
                    })
                    .catch(fehler => {
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                },
                auswahlEntleihen: function(schuelerListe, auswahlUUIDs, buchID) {
                    var deferred = $q.defer();
                    var queries = [];
                    for(var i=0;i<schuelerListe.length;i++) {
                        //schuelerListe: Datenformat [{schueler: Schüler-Objekt aus Datenbank, ausgeliehen: true/false}]
                        var schuelerObjekt = schuelerListe[i];
                        if(auswahlUUIDs.includes(schuelerObjekt.schueler.uuid)) {
                            if(schuelerObjekt.ausgeliehen) {
                                queries.push(this.delete(schuelerObjekt.schueler.uuid, buchID));
                            }
                        }
                    }
                    $q.all(queries)
                    .then(_ => {
                        deferred.resolve();
                    })
                    .catch(fehler => {
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                },
                ladeDeputat: function(auswahl) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/deputate/' + auswahl.fach.uuid + '/' + auswahl.klasse.uuid
                    })
                    .then(antwort => {
                        if(antwort.data.error === false) {
                            deferred.resolve(antwort.data.name);
                        } else {
                            deferred.reject(antwort);
                        }
                    })
                    .catch(f => {
                        deferred.reject(f);
                    });
                    return deferred.promise;
                },
                buchDaten: function(auswahl, buchUUID) {
                    var deferred = $q.defer();
                    const klasse_id = auswahl.klasse.uuid;
                    const fach_id = auswahl.fach.uuid;
                    const buch_id = buchUUID;
                    $http.get('/ausleihe/anzahl/kfb/' + klasse_id + '/' + fach_id + '/' + buch_id)
                    .then(data => {
                        deferred.resolve(data.data);
                    }, fehler => {
                        deferred.reject(fehler);
                    })
                    return deferred.promise;
                }
            };
        }
})();
