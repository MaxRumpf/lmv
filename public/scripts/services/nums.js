(function() {
    'use strict';
    angular
        .module('app')
        .service('NumService', NumService);
        NumService.$inject = [];
        function NumService() {
            return {
                DEzuEN: function(de) {
                    if(!de) {
                        return null;
                    }
                    return de.toString().replace(',', '.');
                },
                ENzuDE: function(en) {
                    if(!en) {
                        return null;
                    }
                    return en.toString().replace('.', ',');
                }
            };
        }
})();
