(function() {
    'use strict';
    angular
        .module('app')
        .service('BuecherService', BuecherService);
        BuecherService.$inject = ['$http', '$q']
        function BuecherService($http, $q) {
            return {
                aktualisiereBuch: function(buch, erfolgreichCallback, fehlerCallback) {
                	$http({
                		method: 'PUT',
                		url: '/buecher/' + buch.uuid,
                		data: buch
                	}).then(erfolgreichCallback, fehlerCallback);
                },
                loescheBuch: function(buch, erfolgreichCallback, fehlerCallback) {
                	console.log(buch);
                	$http({
                		method: 'DELETE',
                		url: '/buecher/' + buch.uuid
                	}).then(erfolgreichCallback, fehlerCallback);
                }
            };
        }
})();
