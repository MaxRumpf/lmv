(function() {
    'use strict';
    angular
        .module('app')
        .service('DateService', DateService);
        DateService.$inject = [];
        function DateService() {
            return {
                DEzuEN: function(de) {
                    if(de.length != 10) {
                        return null;
                    }
                    return de.split('.').reverse().join('-');
                },
                ENzuDE: function(en) {
                    if(en.length != 10) {
                        return null;
                    }
                    return en.split('-').reverse().join('.');
                }
            };
        }
})();
