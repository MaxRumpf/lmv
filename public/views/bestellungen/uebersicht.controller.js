(function() {
    'use strict';
    angular
        .module('app')
        .controller('BestellungenÜbersichtCtrl', BestellungenÜbersichtCtrl);
        BestellungenÜbersichtCtrl.$inject = ['$scope', '$http', 'DateService', 'NumService', 'MultiHTTP'];
        function BestellungenÜbersichtCtrl($scope, $http, DateService, NumService, MultiHTTP) {
            
            $scope.einstellungen = {
                neueSchueler: 0,
                neueRESchueler: 0,
                neueRKSchueler: 0,
                neueETSchueler: 0
            };

            $scope.data = {
                faecher: []
            };

            $scope.auswahl = {
                fach: {}
            };

            $scope.anzeige = {
                buecher: []
            };

            $scope.$watch('auswahl', function() {
                if($scope.auswahl.fach.uuid) {
                    console.log('auswahl triggered');
                    $scope.ladeBuecher();
                }
            }, true);
           
            $scope.ladeEinstellungen = function() {
                $http.get('/einstellungen')
                .then(antwort => {
                    antwort.data.einstellungen.forEach(wertePaar => {
                        console.log(wertePaar);
                        $scope.einstellungen[wertePaar.schlüssel] = +wertePaar.wert;
                    })
                    console.log($scope.einstellungen);
                })
            }

            $scope.ladeBuecher = function() {
                var fach_id = $scope.auswahl.fach.uuid;
                $http.get('/buecher/bestellungen/' + fach_id)
                .then(antwort => {
                    if(antwort.data.error === false) {
                        $scope.anzeige.buecher = antwort.data.daten;
                        console.log($scope.anzeige.buecher);
                    } else {
                        alert('Bücher-Liste konnte nicht geladen werden.');
                    }
                })
            }

            $scope.ladeEinstellungen();

            $scope.einstellungenSpeichern = function() {
                var queries = [];
                for(var schlüssel in $scope.einstellungen) {
                    if($scope.einstellungen.hasOwnProperty(schlüssel)) {
                        queries.push($http({
                            method: 'POST',
                            url: '/einstellungen',
                            data: {
                                schluessel: schlüssel,
                                wert: $scope.einstellungen[schlüssel]
                            }
                        }));
                    }
                }
                Promise.all(queries)
                .then(_ => {
                    $scope.ladeEinstellungen();
                })
                .catch(fehler => {
                    alert('Konnte Einstellungen nicht abspeichern');
                })
            }

            $scope.ladeDaten = function() {
                MultiHTTP.get(
                    [
                        '/faecher',
                        '/buecher'
                    ]
                )
                .then(antwort => {
                    $scope.data.faecher = antwort['/faecher'];
                    $scope.data.buecher = antwort['/buecher'];
                })
            }
            
            $scope.setzeAuswahl = function(typ, wert) {
                switch(typ) {
                    case 'fach':
                        $scope.auswahl.fach = wert;
                        break;
                    case 'buch':
                        $scope.auswahl.buch = wert;
                        break;
                    default: break;
                }
            }
            
            $scope.ladeDaten();

		}
})();
